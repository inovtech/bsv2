<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BoutiqueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boutiques = [
            [
                'nom_boutique' => 'Nom Boutique',
                'user_boutique_id' => 1,
                'slug_nom_boutique' => "nom_boutique",
                'categorie_boutique_id' => 1,
                'adresse_boutique' => 'Adresse',
                'ville_boutique' => "Dakar",
                'telephone_boutique' => "777777777",
                'email_boutique' => "email@boutique1.sn",
                'description_boutique' => "description boutique",
                'photo_boutique' => null,
                'pack_boutique_id' => 1,
                'is_active_boutique' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_boutique' => 'Nom Boutique 2',
                'user_boutique_id' => 2,
                'slug_nom_boutique' => "nom_boutique 2",
                'categorie_boutique_id' => 1,
                'adresse_boutique' => 'Adresse',
                'ville_boutique' => "Dakar",
                'telephone_boutique' => "777777777",
                'email_boutique' => "email@boutique2.sn",
                'description_boutique' => "description boutique",
                'photo_boutique' => null,
                'pack_boutique_id' => 1,
                'is_active_boutique' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_boutique' => 'Nom Boutique 3',
                'user_boutique_id' => 3,
                'slug_nom_boutique' => "nom_boutique 3",
                'categorie_boutique_id' => 1,
                'adresse_boutique' => 'Adresse',
                'ville_boutique' => "Dakar",
                'telephone_boutique' => "777777777",
                'email_boutique' => "email@boutique3.sn",
                'description_boutique' => "description boutique",
                'photo_boutique' => null,
                'pack_boutique_id' => 3,
                'is_active_boutique' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_boutique' => 'Nom Boutique 4',
                'user_boutique_id' => 4,
                'slug_nom_boutique' => "nom_boutique 4",
                'categorie_boutique_id' => 1,
                'adresse_boutique' => 'Adresse',
                'ville_boutique' => "Dakar",
                'telephone_boutique' => "777777777",
                'email_boutique' => "email@boutique4.sn",
                'description_boutique' => "description boutique",
                'photo_boutique' => null,
                'pack_boutique_id' => 2,
                'is_active_boutique' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ];
        DB::table('boutiques')->insert($boutiques);
    }
}
