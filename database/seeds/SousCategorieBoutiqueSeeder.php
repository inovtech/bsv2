<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SousCategorieBoutiqueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catb = [
            [
                'nom_sous_categorie_boutique' => 'Gâteaux',
                'slug_sous_categorie_boutique' => 'gâteaux',
                'categorie_boutique_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Viande & Poulet',
                'slug_sous_categorie_boutique' => 'viande-&-poulet',
                'categorie_boutique_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Appareil photo & cameras',
                'slug_sous_categorie_boutique' => 'appareil-photo-&-cameras',
                'categorie_boutique_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Ordinateur',
                'slug_sous_categorie_boutique' => 'ordinateur',
                'categorie_boutique_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Accessoires femme',
                'slug_sous_categorie_boutique' => 'accessoires-femme',
                'categorie_boutique_id' => 5,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Chaussure femme',
                'slug_sous_categorie_boutique' => 'chaussure femme',
                'categorie_boutique_id' => 5,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Prêt à porter femme',
                'slug_sous_categorie_boutique' => 'prêt-à-porter-femme',
                'categorie_boutique_id' => 5,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Tenus traditionnel femme',
                'slug_sous_categorie_boutique' => 'tenus-traditionnel-femme',
                'categorie_boutique_id' => 5,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Accessoires homme',
                'slug_sous_categorie_boutique' => 'accessoires-homme',
                'categorie_boutique_id' => 6,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Chaussure homme',
                'slug_sous_categorie_boutique' => 'chaussure-homme',
                'categorie_boutique_id' => 6,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Prêt à porter homme',
                'slug_sous_categorie_boutique' => 'prêt-à-porter-homme',
                'categorie_boutique_id' => 6,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'tenus traditionnel homme',
                'slug_sous_categorie_boutique' => 'tenus-traditionnel-homme',
                'categorie_boutique_id' => 6,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Vaiselle',
                'slug_sous_categorie_boutique' => 'vaiselle',
                'categorie_boutique_id' => 7,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'Maquillage',
                'slug_sous_categorie_boutique' => 'maquillage',
                'categorie_boutique_id' => 8,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_sous_categorie_boutique' => 'parfums',
                'slug_sous_categorie_boutique' => 'tenus-traditionnel-femme',
                'categorie_boutique_id' => 8,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
            ,

            [
                'nom_sous_categorie_boutique' => 'Produit Capillaire',
                'slug_sous_categorie_boutique' => 'produit-capillaire',
                'categorie_boutique_id' => 8,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
            ,

            [
                'nom_sous_categorie_boutique' => 'Univers homme',
                'slug_sous_categorie_boutique' => 'univers-homme',
                'categorie_boutique_id' => 8,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
            ,

            [
                'nom_sous_categorie_boutique' => 'Accessoires Téléphones',
                'slug_sous_categorie_boutique' => 'accessoires-téléphones',
                'categorie_boutique_id' => 10,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];
        DB::table('sous_categorie_boutiques')->insert($catb);
    }
}
