<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CategorieBoutiqueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catb = [
            [
                'nom_categorie_boutique' => 'Alimentaire',
                'slug_categorie_boutique' => 'alimentaire',
                'photo_categorie_boutique' => "https://www.boutiquesenegal.com/public/storage/categorie_boutique/1610541085.jpg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_boutique' => 'Bijoux',
                'slug_categorie_boutique' => 'bijoux',
                'photo_categorie_boutique' => "https://www.boutiquesenegal.com/public/storage/categorie_boutique/1610541061.jpg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_boutique' => 'Electronique',
                'slug_categorie_boutique' => 'electronique',
                'photo_categorie_boutique' => "https://www.boutiquesenegal.com/public/storage/categorie_boutique/1610541108.jpg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_boutique' => 'Mode Enfant',
                'slug_categorie_boutique' => 'mode-enfant',
                'photo_categorie_boutique' => "https://www.boutiquesenegal.com/public/storage/categorie_boutique/1610541132.jpg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_boutique' => 'Mode Femme',
                'slug_categorie_boutique' => 'mode-femme',
                'photo_categorie_boutique' => "https://www.boutiquesenegal.com/public/storage/categorie_boutique/1610541148.jpg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_boutique' => 'Mode Homme',
                'slug_categorie_boutique' => 'mode-homme',
                'photo_categorie_boutique' => "https://www.boutiquesenegal.com/public/storage/categorie_boutique/1610541168.jpg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_boutique' => 'Recipient',
                'slug_categorie_boutique' => 'recipient',
                'photo_categorie_boutique' => "https://www.boutiquesenegal.com/public/storage/categorie_boutique/1610541185.jpg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_boutique' => 'Santé et Beauté',
                'slug_categorie_boutique' => 'santé-et-beauté',
                'photo_categorie_boutique' => "https://www.boutiquesenegal.com/public/storage/categorie_boutique/1610541351.jpg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_boutique' => 'Sport',
                'slug_categorie_boutique' => 'sport',
                'photo_categorie_boutique' => "https://www.boutiquesenegal.com/public/storage/categorie_boutique/1610541371.jpg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_boutique' => 'Téléphones et Tablettes',
                'slug_categorie_boutique' => 'téléphones-et,-tablettes',
                'photo_categorie_boutique' => "https://www.boutiquesenegal.com/public/storage/categorie_boutique/1610541400.jpg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_boutique' => 'Tissu',
                'slug_categorie_boutique' => 'tissu',
                'photo_categorie_boutique' => "https://www.boutiquesenegal.com/public/storage/categorie_boutique/1610541447.jpg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];
        DB::table('categorie_boutiques')->insert($catb);
    }
}
