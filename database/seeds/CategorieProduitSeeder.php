<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CategorieProduitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catp = [
            [
                'nom_categorie_produit' => 'Maquillage',
                'slug_categorie_produit' => 'maquillage',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Outils & Accessoires Beauté',
                'slug_categorie_produit' => 'outils-accessoires-beaute',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Parfums de luxe',
                'slug_categorie_produit' => 'parfums-de-luxe',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Soins bucco-dentaires',
                'slug_categorie_produit' => 'soins-bucco-dentaires',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Soin de la peau',
                'slug_categorie_produit' => 'soin-de-la-peau',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Soin des cheveux',
                'slug_categorie_produit' => 'soin-des-cheveux',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Protéines',
                'slug_categorie_produit' => 'proteines',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Bijoux',
                'slug_categorie_produit' => 'bijoux',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Montres',
                'slug_categorie_produit' => 'montres',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Chaussures',
                'slug_categorie_produit' => 'chaussures',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Accessoires Téléphone',
                'slug_categorie_produit' => 'accessoires-telephone',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Jeux vidéos & Consoles',
                'slug_categorie_produit' => 'jeux-videos-consoles',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Produits Bébé',
                'slug_categorie_produit' => 'produits-bebe',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Électroménager',
                'slug_categorie_produit' => 'electromenager',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Sacs à main',
                'slug_categorie_produit' => 'sacs-a-main',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Costumes',
                'slug_categorie_produit' => 'costumes',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Vêtements traditionnels',
                'slug_categorie_produit' => 'vetements-traditionnels',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Botte (chaussure)',
                'slug_categorie_produit' => 'botte-chaussure',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Ballerine',
                'slug_categorie_produit' => 'ballerine',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Vêtements homme',
                'slug_categorie_produit' => 'vetements-homme',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nom_categorie_produit' => 'Vêtements femme',
                'slug_categorie_produit' => 'Vêtements femme',
                'photo_categorie_produit' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];
        DB::table('categorie_produits')->insert($catp);
    }
}
