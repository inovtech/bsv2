<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackAbonnementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pack_abonnements', function (Blueprint $table) {
            $table->id();
            $table->string('nom_pack_abonnement')->unique();
            $table->text('description_pack_abonnement')->nullable();
            $table->integer('prix_pack_abonnement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pack_abonnements');
    }
}
