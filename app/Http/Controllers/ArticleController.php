<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Article;
use App\Models\CategorieBlog;
use App\Models\Tag;
use Auth;
use Session;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::paginate(10);

        return view('Admin.Article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = CategorieBlog::all();
        $tags = Tag::all();
        return view('Admin.Article.create', compact('categories','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'titre_article' => 'required',
            'description_article' => 'required',
            'categorie_id' => 'required',
            // 'auteur' => 'required',
        ]);

        $article = Article::create([
            'titre_article' => $request->titre_article,
            'slug_article' => Str::slug($request->titre_article, '-'),
            'description_article' => $request->description_article,
            'photo_article' => '',
            'categorie_id' => $request->categorie_id,
            'auteur' => Auth::id(),
        ]);
        $article->tags()->attach($request->tags);
        if($request->hasFile('photo_article')){
            $photo = $request->photo_article;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/article/', $image_new_name);
            $article->photo_article = '/public/storage/article/'.$image_new_name;
        }

        $article->save();

        Session::flash('success', 'Une nouveau article de blog vient d\'être crée avec succès');

        return redirect()->route('article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::where('id', $id)->first();
        $categories = CategorieBlog::all();
        // dd($cat_boutiques);
        $tags = Tag::all();
        return view('Admin.Article.edit', compact('article', 'tags','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::where('id', $id)->first();

        $this->validate($request, [
            'titre_article' => "required",
            'description_article' => 'required',
            'categorie_id' => 'required',
            // 'auteur' => 'required',
        ]);

        $article->tags()->sync($request->tags);
        if($request->hasFile('photo_article')){
            $photo = $request->photo_article;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/article/', $image_new_name);
            $article->photo_article = '/public/storage/article/'.$image_new_name;
        }

        $article->titre_article = $request->titre_article;
        $article->slug_article = Str::slug($request->titre_article, '-');
        $article->description_article = $request->description_article;
        $article->categorie_id = $request->categorie_id;
        $article->auteur = Auth::id();

        $article->save();
        Session::flash('success', 'Un Pack Abonnement vient d\'être modifié avec succès');

        return redirect()->route('article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();

        Session::flash('success', 'un article a été supprimé avec succès');
        return redirect()->route('article.index');
    }
}
