<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Boutique;
use App\Models\Produit;
use App\Models\PhotoProduit;
use App\Models\PackAbonnement;
use App\Models\CategorieBoutique;
use App\Models\CategorieProduit;
use App\Models\GallerieBoutique;
use App\User;

use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_boutique()
    {
        $boutiques = Boutique::join('pack_abonnements', 'boutiques.pack_boutique_id', 'pack_abonnements.id')
                            ->where('user_boutique_id', auth()->user()->id)
                            ->select('boutiques.*', 'pack_abonnements.nom_pack_abonnement')->paginate(10);
        $count_product_shop = Produit::join('boutiques', 'produits.boutique_id', 'boutiques.id')
                                    ->where('boutiques.user_boutique_id', auth()->user()->id)->get();
        return view('Client.boutique.index', compact('boutiques', 'count_product_shop'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function single_boutique($slug_nom_boutique)
    {
        $boutique = Boutique::join('pack_abonnements', 'boutiques.pack_boutique_id', 'pack_abonnements.id')
                            ->where([['boutiques.slug_nom_boutique', $slug_nom_boutique],['user_boutique_id', auth()->user()->id]])
                            ->select('boutiques.*', 'pack_abonnements.nom_pack_abonnement')->first();
        return view('Client.boutique.show', compact('boutique'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_produit()
    {
        $produits = Produit::join('categorie_produits', 'produits.categorie_produit_id', 'categorie_produits.id')
                            ->join('boutiques', 'produits.boutique_id', 'boutiques.id')
                            ->where('boutiques.user_boutique_id', auth()->user()->id)
                            ->select('produits.*', 'boutiques.nom_boutique', 'categorie_produits.nom_categorie_produit')->paginate(10);
        return view('Client.Produit.index', compact('produits'));
    }

    /**
     * Display user informations of the user auth's.
     *
     * @return \Illuminate\Http\Response
     */
    public function profil()
    {
        $user = auth()->user();
        $user_id = $user->id;

        $profil = User::join('roles', 'users.role_id', 'roles.id')->where('users.id', $user_id)->first();
        return view('Client.profil', compact('profil'));
    }

    /**
     * Reset User password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_pwd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ancien_mot_de_passe' => 'required',
            'password' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[@$!%*#?&]).*$/|confirmed',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect(route('client.profil'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = auth()->user();
        if(Hash::check($request->ancien_mot_de_passe, $user->password)){
            $user->fill([
                'password' => Hash::make($request->password),
            ])->save();

            Session::flash('success', 'Le mot de passe a été mis à jour');
            return redirect()->to(route('client.profil'));
        } else{
            Session::flash('warning', 'L\'ancien mot de passe ne correspond pas');
            return redirect()->to(route('client.profil'));
        }
    }

    /**
     * Update the auth user's profile information
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_auth_profile(Request $request)
    {
        $user = auth()->user();

        $this->validate($request, [
            'prenom_user' => 'required|string|max:255',
            'nom_user' => 'required|string|max:255',
            'email' => "required|email|unique:users,email, $user->id",
            'adresse_user' => 'required',
            'telephone_user' => 'required',
        ]);

        $user->prenom_user = $request->prenom_user;
        $user->nom_user = $request->nom_user;
        $user->email = $request->email;
        $user->adresse_user = $request->adresse_user;
        $user->telephone_user = $request->telephone_user;

        if($request->hasFile('photo')){
            $photo = $request->profile_photo_path;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/users/', $image_new_name);
            $user->profile_photo_path = '/public/storage/users/'.$image_new_name;
        }

        $user->save();
        Session::flash('success', 'Votre profil a été mise à jour avec succès');

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_boutique()
    {
        $categories = CategorieBoutique::all();
        $packs = PackAbonnement::all();
        return view('Client.boutique.create', compact('categories', 'packs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_produit()
    {
        $boutiques = Boutique::where('user_boutique_id', auth()->user()->id)->get();
        $categories = CategorieProduit::orderBy("nom_categorie_produit")->get();
        return view('Client.Produit.create', compact('boutiques', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_boutique(Request $request)
    {
        $this->validate($request, [
            'categorie_boutique_id' => 'required',
            'nom_boutique' => 'required|unique:boutiques,nom_boutique',
            'adresse_boutique' => 'required',
            'ville_boutique' => 'required',
            'telephone_boutique' => 'required',
            'email_boutique' => 'required',
            'description_boutique' => 'required',
            'pack_boutique_id' => 'required',
        ]);

        $boutique = Boutique::create([
            'user_boutique_id' => auth()->user()->id,
            'categorie_boutique_id' => $request->categorie_boutique_id,
            'nom_boutique' => $request->nom_boutique,
            'slug_nom_boutique' => Str::slug($request->nom_boutique, '-'),
            'adresse_boutique' => $request->adresse_boutique,
            'ville_boutique' => $request->ville_boutique,
            'telephone_boutique' => $request->telephone_boutique,
            'email_boutique' => $request->email_boutique,
            'site_web_boutique' => $request->site_web_boutique,
            'link_facebook_boutique' => $request->link_facebook_boutique,
            'link_instagram_boutique' => $request->link_instagram_boutique,
            'link_twitter_boutique' => $request->link_twitter_boutique,
            'jour_ouvrable_boutique' => $request->jour_ouvrable_boutique,
            'open_time_boutique' => $request->open_time_boutique,
            'close_time_boutique' => $request->close_time_boutique,
            'description_boutique' => $request->description_boutique,
            'map_url_boutique' => $request->map_url_boutique,
            'pack_boutique_id' => $request->pack_boutique_id,
            'is_active_boutique' => false,
        ]);

        if($request->hasFile('photo_boutique')){
            $photo = $request->photo_boutique;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/boutique/', $image_new_name);
            $boutique->photo_boutique = '/public/storage/boutique/'.$image_new_name;
        }

        if ($request->hasFile('galleries')) {
            $images = $request->galleries;
            foreach ($images as $img) {
                $image_new_name = Str::random(10) . '.' . $img->getClientOriginalExtension();
                $img->move('public/storage/boutique/gallerie/', $image_new_name);
                GallerieBoutique::create(
                    [
                        'boutique_id' =>  $boutique->id,
                        'photo_gallerie' => '/public/storage/boutique/gallerie/'.$image_new_name,
                    ]
                );
            }
        }

        $boutique->save();
        Session::flash('success', 'Votre boutique vient d\'être crée avec succès.Veillez patienter au moins 24h afin que l’équipe examine votre demande !');

        return redirect()->route('client.index-shop');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_produit(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'nom_produit' => 'required',
            'boutique_id' => 'required',
            'categorie_produit_id' => 'required',
            'prix_produit' => 'required',
        ]);

        $produit = Produit::create([
            'nom_produit' => $request->nom_produit,
            'slug_nom_produit' => Str::slug($request->nom_produit, '-'),
            'boutique_id' => $request->boutique_id,
            'categorie_produit_id' => $request->categorie_produit_id,
            'description_produit' => $request->description_produit,
            'prix_produit' => $request->prix_produit,
            'commander_produit' => $request->commander_produit,
        ]);

        if($request->hasFile('photo_produit')){
            $photo = $request->photo_produit;
            $image_new_name = rand() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/boutique/produits/', $image_new_name);
            $produit->photo_produit = '/public/storage/boutique/produits/'.$image_new_name;
        }

        $produit->save();
        Session::flash('success', 'Un nouveau produit vient d\'être crée avec succès');

        return redirect()->route('client.index-produit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_boutique($id)
    {
        $boutique = Boutique::where('id', $id)->first();
        $categories = CategorieBoutique::all();
        $packs = PackAbonnement::all();
        $gallerieImages = GallerieBoutique::where('boutique_id', $id)->get();
        $users = User::join('roles', 'users.role_id', 'roles.id')->where('code_role', 'CL')->get();
        return view('Client.boutique.edit', compact('gallerieImages', 'categories', 'packs', 'users', 'boutique'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_boutique(Request $request)
    {

        $boutique = Boutique::where('id', $request->id)->first();
        $this->validate($request, [
            'categorie_boutique_id' => 'required',
            'nom_boutique' => "required|unique:boutiques,nom_boutique,$boutique->id",
            'adresse_boutique' => 'required',
            'ville_boutique' => 'required',
            'telephone_boutique' => 'required',
            'email_boutique' => 'required',
            'description_boutique' => 'required',
            'pack_boutique_id' => 'required',
        ]);


        $boutique->user_boutique_id = auth()->user()->id;
        $boutique->categorie_boutique_id = $request->categorie_boutique_id;
        $boutique->nom_boutique = $request->nom_boutique;
        $boutique->slug_nom_boutique = Str::slug($request->nom_boutique, '-');
        $boutique->adresse_boutique = $request->adresse_boutique;
        $boutique->ville_boutique = $request->ville_boutique;
        $boutique->telephone_boutique = $request->telephone_boutique;
        $boutique->email_boutique = $request->email_boutique;
        $boutique->site_web_boutique = $request->site_web_boutique;
        $boutique->link_facebook_boutique = $request->link_facebook_boutique;
        $boutique->link_instagram_boutique = $request->link_instagram_boutique;
        $boutique->link_twitter_boutique = $request->link_twitter_boutique;
        $boutique->jour_ouvrable_boutique = $request->jour_ouvrable_boutique;
        $boutique->open_time_boutique = $request->open_time_boutique;
        $boutique->close_time_boutique = $request->close_time_boutique;
        $boutique->description_boutique = $request->description_boutique;
        $boutique->map_url_boutique = $request->map_url_boutique;
        $boutique->pack_boutique_id = $request->pack_boutique_id;
        $boutique->is_active_boutique = false;

        if($request->hasFile('photo_boutique')){
            $photo = $request->photo_boutique;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/boutique/', $image_new_name);
            $boutique->photo_boutique = '/public/storage/boutique/'.$image_new_name;
        }

        if ($request->hasFile('galleries')) {
            $images = $request->galleries;
            foreach ($images as $img) {
                $image_new_name = Str::random(10) . '.' . $img->getClientOriginalExtension();
                $img->move('public/storage/boutique/gallerie/', $image_new_name);
                GallerieBoutique::create(
                    [
                        'boutique_id' =>  $boutique->id,
                        'photo_gallerie' => '/public/storage/boutique/gallerie/'.$image_new_name,
                    ]
                );
            }
        }

        $boutique->save();
        Session::flash('success', 'La boutique vient d\'être modifiée avec succès');

        return redirect()->route('client.index-shop');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_produit($id)
    {
        $produit = Produit::where('id', $id)->first();
        $boutiques = Boutique::where('user_boutique_id', auth()->user()->id)->get();
        $cat_produits = CategorieProduit::orderBy("nom_categorie_produit")->get();
        return view('Client.Produit.edit', compact('produit', 'boutiques', 'cat_produits'));
    }
     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_produit(Request $request)
    {
        $produit = Produit::where('id', $request->id)->first();

        $this->validate($request, [
            'nom_produit' => 'required',
            'boutique_id' => 'required',
            'categorie_produit_id' => 'required',
            'prix_produit' => 'required',
        ]);

        $produit->nom_produit = $request->nom_produit;
        $produit->slug_nom_produit = Str::slug($request->nom_produit, '-');
        $produit->boutique_id = $request->boutique_id;
        $produit->categorie_produit_id = $request->categorie_produit_id;
        $produit->prix_produit = $request->prix_produit;
        $produit->description_produit = $request->description_produit;

        if($request->hasFile('photo_produit')){
            $photo = $request->photo_produit;
            $image_new_name = rand() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/boutique/produits/', $image_new_name);
            $produit->photo_produit = '/public/storage/boutique/produits/'.$image_new_name;
        }

        $produit->save();
        Session::flash('success', 'Votre produit vient d\'être modifié avec succès');

        return redirect()->route('client.index-produit');

    }


    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function removeImageFromGallerie(Request $request)
    {
        // dd($request->id);
        $image = GallerieBoutique::where('id', $request->id)->delete();

        if ($image) {
            return response()->json( array('success'=>true) );
        }

        return response()->json( array('success'=>false) );
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_produit($id)
    {
        $produit = Produit::find($id);
        $produit->delete();

        Session::flash('success', 'un produit a été supprimé avec succès');
        return redirect()->route('client.index-produit');
    }
}
