<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PackAbonnement;

use Session;

class PackAbonnementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packs = PackAbonnement::paginate(10);

        return view('Admin.Pack.index', compact('packs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Pack.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nom_pack_abonnement' => 'required|unique:pack_abonnements,nom_pack_abonnement',
            'prix_pack_abonnement' => 'required',
        ]);

        PackAbonnement::create([
            'nom_pack_abonnement' => $request->nom_pack_abonnement,
            'prix_pack_abonnement' => $request->prix_pack_abonnement,
            'description_pack_abonnement' => $request->description_pack_abonnement,
        ]);
        Session::flash('success', 'Un nouveau Pack Abonnement vient d\'être crée avec succès');

        return redirect()->route('pack-abonnement.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pack = PackAbonnement::where('id', $id)->first();
        return view('Admin.Pack.edit', compact('pack'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pack = PackAbonnement::where('id', $id)->first();

        $this->validate($request, [
            'nom_pack_abonnement' => "required|unique:pack_abonnements,nom_pack_abonnement,$pack->id",
            'prix_pack_abonnement' => 'required',
        ]);

        $pack->nom_pack_abonnement = $request->nom_pack_abonnement;
        $pack->prix_pack_abonnement = $request->prix_pack_abonnement;
        $pack->description_pack_abonnement = $request->description_pack_abonnement;
        $pack->save();
        Session::flash('success', 'Un Pack Abonnement vient d\'être modifié avec succès');

        return redirect()->route('pack-abonnement.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pack = PackAbonnement::find($id);
        $pack->delete();

        Session::flash('success', 'Le Pack a été supprimé avec succès');
        return redirect()->route('pack-abonnement.index');
    }
}
