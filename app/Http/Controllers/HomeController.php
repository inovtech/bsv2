<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Boutique;
use App\Models\Produit;
use App\Models\PackAbonnement;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $count_shop = Boutique::where('user_boutique_id', auth()->user()->id)->get();
        $boutiques = Boutique::join('categorie_boutiques', 'boutiques.categorie_boutique_id', 'categorie_boutiques.id')
                        ->where('boutiques.user_boutique_id', auth()->user()->id)->get();
        $count_wait_shop = Boutique::where([
            ['user_boutique_id', auth()->user()->id],
            ['is_active_boutique', false]
            ])->get();
        $count_premium_shop = Boutique::where([
            ['user_boutique_id', auth()->user()->id],
            ['pack_boutique_id', 3]
            ])->get();
        $count_product_shop = Produit::join('boutiques', 'produits.boutique_id', 'boutiques.id')
            ->where('boutiques.user_boutique_id', auth()->user()->id)->get();
        return view('Client.dashboard', compact('count_shop', 'count_wait_shop', 'count_premium_shop', 'count_product_shop', 'boutiques'));
    }

    public function admin()
    {
        $count_shop = Boutique::all();
        $count_wait_shop = Boutique::where('is_active_boutique', false)->get();
        $count_premium_shop = Boutique::where('pack_boutique_id', 3)->get();
        $count_product_shop = Produit::all();
        $packs = PackAbonnement::all();
        return view('Admin.dashboard', compact('packs', 'count_shop', 'count_wait_shop', 'count_premium_shop', 'count_product_shop'));
    }
}
