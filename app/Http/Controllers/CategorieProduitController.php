<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\CategorieProduit;

use Session;

class CategorieProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $cat_produits = CategorieProduit::paginate(10);

        return view('Admin.CategorieProduit.index', compact('cat_produits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.CategorieProduit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nom_categorie_produit' => 'required|unique:categorie_produits,nom_categorie_produit',
        ]);

        $categorie = CategorieProduit::create([
            'nom_categorie_produit' => $request->nom_categorie_produit,
            // 'photo_categorie_produit' => $request->photo_categorie_produit,
            'slug_categorie_produit' => Str::slug($request->nom_categorie_produit, '-'),
        ]);
        if($request->hasFile('photo_categorie_produit')){
            $photo = $request->photo_categorie_produit;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/categorie_produit/', $image_new_name);
            $categorie->photo_categorie_produit = '/public/storage/categorie_produit/'.$image_new_name;
        }

        $categorie->save();

        Session::flash('success', 'Une nouvelle categorie de produit vient d\'être crée avec succès');

        return redirect()->route('categorie-produit.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorie = CategorieProduit::where('id', $id)->first();
        return view('Admin.CategorieProduit.edit', compact('categorie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categorie = CategorieProduit::where('id', $id)->first();

        $this->validate($request, [
            'nom_categorie_produit' => "required|unique:categorie_produits,nom_categorie_produit, ",
        ]);


            $categorie->nom_categorie_produit = $request->nom_categorie_produit;
            $categorie->slug_categorie_produit = Str::slug($request->nom_categorie_produit, '-');

        if($request->hasFile('photo_categorie_produit')){
            $photo = $request->photo_categorie_produit;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/categorie_produit/', $image_new_name);
            $categorie->photo_categorie_produit = '/public/storage/categorie_produit/'.$image_new_name;
        }

        $categorie->save();

        Session::flash('success', 'Une Pack categorie vient d\'être modifié avec succès');

        return redirect()->route('categorie-produit.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categorie = CategorieProduit::find($id);
        $categorie->delete();

        Session::flash('success', 'La Categorie a été supprimé avec succès');
        return redirect()->route('categorie-produit.index');
    }
}
