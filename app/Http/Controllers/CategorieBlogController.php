<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\CategorieBlog;
use Session;

class CategorieBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cat_blogs = CategorieBlog::paginate(10);

        return view('Admin.CategorieBlog.index', compact('cat_blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.CategorieBlog.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nom_categorie_blog' => 'required|unique:categorie_blogs,nom_categorie_blog',
        ]);

        CategorieBlog::create([
            'nom_categorie_blog' => $request->nom_categorie_blog,
            'slug_categorie_blog' => Str::slug($request->nom_categorie_blog, '-'),
        ]);
        Session::flash('success', 'Une nouvelle categorie de blog vient d\'être crée avec succès');

        return redirect()->route('categorie-blog.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat_blog = CategorieBlog::where('id', $id)->first();
        return view('Admin.CategorieBlog.edit', compact('cat_blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat_blog = CategorieBlog::where('id', $id)->first();

         $this->validate($request, [
            'nom_categorie_blog' => "required|unique:categorie_blogs,nom_categorie_blog,$cat_blog->id",
        ]);

        $cat_blog->nom_categorie_blog = $request->nom_categorie_blog;
        $cat_blog->slug_categorie_blog = Str::slug($request->nom_categorie_blog, '-');
        $cat_blog->save();
        Session::flash('success', 'Une categorie vient d\'être modifié avec succès');

        return redirect()->route('categorie-blog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat_blog = CategorieBlog::find($id);
        $cat_blog->delete();

        Session::flash('success', 'Lea Categorie Blog a été supprimé avec succès');
        return redirect()->route('categorie-blog.index');
    }
}
