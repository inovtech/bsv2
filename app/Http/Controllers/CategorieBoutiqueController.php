<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\CategorieBoutique;

use Session;

class CategorieBoutiqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cat_boutiques = CategorieBoutique::paginate(10);

        return view('Admin.CategorieBoutique.index', compact('cat_boutiques'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.CategorieBoutique.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nom_categorie_boutique' => 'required|unique:categorie_boutiques,nom_categorie_boutique',
        ]);

       $cat_boutiques = CategorieBoutique::create([
            'nom_categorie_boutique' => $request->nom_categorie_boutique,
            'slug_categorie_boutique' => Str::slug($request->nom_categorie_boutique, '-'),
            ]);

        if($request->hasFile('photo_categorie_boutique')){
        $photo = $request->photo_categorie_boutique;
        $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
        $photo->move('public/storage/categorie_boutique/', $image_new_name);
        $cat_boutiques->photo_categorie_boutique = '/public/storage/categorie_boutique/'.$image_new_name;
        }

        $cat_boutiques->save();

        Session::flash('success', 'Une nouvelle categorie de boutique vient d\'être crée avec succès');

        return redirect()->route('categorie-boutique.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat_boutiques = CategorieBoutique::where('id', $id)->first();
        return view('Admin.CategorieBoutique.edit', compact('cat_boutiques'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat_boutiques = CategorieBoutique::where('id', $id)->first();

        $this->validate($request, [
            'nom_categorie_boutique' => "required|unique:categorie_boutiques,nom_categorie_boutique,$cat_boutiques->id",
        ]);

        $cat_boutiques->nom_categorie_boutique = $request->nom_categorie_boutique;
        $cat_boutiques->slug_categorie_boutique = Str::slug($request->nom_categorie_boutique, '-');

        if($request->hasFile('photo_categorie_boutique')){
            $photo = $request->photo_categorie_boutique;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/categorie_boutique/', $image_new_name);
            $cat_boutiques->photo_categorie_boutique = '/public/storage/categorie_boutique/'.$image_new_name;
        }

        $cat_boutiques->save();

        Session::flash('success', 'Une Categorie vient d\'être modifié avec succès');

        return redirect()->route('categorie-boutique.index');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat_boutiques = CategorieBoutique::find($id);
        $cat_boutiques->delete();

        Session::flash('success', 'Le Pack a été supprimé avec succès');
        return redirect()->route('categorie-boutique.index');
    }
}
