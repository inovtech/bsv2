<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Tag;
use Session;


class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Tags = Tag::paginate(10);

        return view('Admin.Tag.index', compact('Tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'libelle' => 'required|unique:tags,libelle',
        ]);

        Tag::create([
            'libelle' => $request->libelle,
            'slug' => Str::slug($request->libelle, '-'),
            'description' => $request->description,
        ]);
        Session::flash('success', 'Un nouveau tag vient d\'être crée avec succès');

        return redirect()->route('tag.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::where('id', $id)->first();
        return view('Admin.Tag.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::where('id', $id)->first();

        $this->validate($request, [
            'libelle' =>" required|unique:tags,libelle,$tag->id",
        ]);

        $tag->libelle = $request->libelle;
        $tag->slug = Str::slug($request->libelle, '-');
        $tag->description = $request->description;
        $tag->save();
        Session::flash('success', 'Un tag vient d\'être modifié avec succès');

        return redirect()->route('tag.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::find($id);
        $tag->delete();

        Session::flash('success', 'Le tag a été supprimé avec succès');
        return redirect()->route('tag.index');
    }
}
