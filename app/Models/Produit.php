<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_produit', 'slug_nom_produit', 'boutique_id', 'categorie_produit_id', 'description_produit',
        'prix_produit', 'commander_produit','photo_produit','is_active_produit',
    ];

    public function categorie()
    {
        return $this->belongsTo('App\Models\CategorieProduit', 'categorie_produit_id');
    }

    public function boutique()
    {
        return $this->belongsTo('App\Models\Boutique', 'boutique_id');
    }

}
