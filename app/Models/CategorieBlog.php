<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategorieBlog extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_categorie_blog', 'slug_categorie_blog',
    ];

    public function articles()
    {
        return $this->hasMany('App\Models\Article');
    }
}
