<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SousCategorieBoutique extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_sous_categorie_boutique', 'slug_sous_categorie_boutique', 'categorie_boutique_id'
    ];

    public function categorie()
    {
        return $this->belongsTo('App\Models\CategorieBoutique', 'categorie_boutique_id');
    }
}
