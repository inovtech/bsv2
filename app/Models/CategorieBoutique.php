<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategorieBoutique extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_categorie_boutique', 'slug_categorie_boutique','photo_categorie_boutique',
    ];

    public function souscategorie()
    {
        return $this->hasMany('App\Model\SousCategorieBoutique');
    }

    public function boutiques()
    {
        return $this->hasMany('App\Models\Boutique');
    }
}
