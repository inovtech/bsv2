<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titre_article', 'slug_article', 'description_article', 'photo_article',
        'categorie_id', 'auteur',
    ];

    public function categorie()
    {
        return $this->belongsTo('App\Models\CategorieBlog', 'categorie_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'auteur');
    }
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }
}
