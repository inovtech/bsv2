<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackAbonnement extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_pack_abonnement', 'description_pack_abonnement', 'prix_pack_abonnement'
    ];
}
