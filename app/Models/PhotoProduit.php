<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhotoProduit extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'produit_id', 'photo_produit'
    ];
}
