@extends('layouts.guest')
@section('content')
<header id="header" class="main-header header-float header-sticky header-sticky-smart header-light header-style-03 font-normal">
        <div class="header-wrapper sticky-area">
            <div class="container">
                <nav class="navbar navbar-expand-xl">
                    <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                        <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                            <span></span>
                        </div>
                        <a class="navbar-brand navbar-brand-mobile" href="/">
                            <img src="{{asset('images/white-logo.png')}}" alt="Boutique Senegal" />
                        </a>
                        <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true"
                        data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'>
                            <i class="far fa-search"></i>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse d-xl-flex" id="navbar-main-menu">
                        <a class="navbar-brand d-none d-xl-block" href="/">
                            <img src="{{asset('images/white-logo.png')}}" alt="Boutique Senegal" />
                        </a>
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="/categorie">Categorie <span class="caret"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="/blog">Blog<span class="caret"></i></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/pricing">Offres<span class="caret"></i></span></a>
                                </li>
                                @auth
                                @if (Auth::user()->role_id == 1)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>
                                    
                                @elseif(Auth::user()->role_id == 2)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('home') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>
                                    
                                @endif
                            @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('login') }}"><i class="fas fa-user-circle mr-1"></i> Se
                                    connecter<span class="caret"></i></span></a>
                            </li>
                            @endauth
                                <li class="nav-item ">
                                    <a class="btn btn-primary text-capitalize teest" href="{{route('client.create-shop')}}"> + Ajoutez votre boutique<span class="caret"></i></span></a>
                                </li>
                            </ul>
                    </div>
                </nav>
            </div>
        </div>
</header>
  <div id="page-title" class="page-title page-title-style-background">
    <div class="container">
      <div class="h-100 d-flex flex-column justify-content-center text-center">
        <h1 class="mb-0" data-animate="fadeInDown">
          <span class="font-weight-light">Notre </span>
          <span class="bleu">Blog</span>
        </h1>
        <ul
          class="breadcrumb breadcrumb-style-01 justify-content-center"
          data-animate="fadeInUp">
          <li class="breadcrumb-item">
            <a href="{{url('/')}}" class="link-hover-dark-primary">Accueil</a>
          </li>
          <li class="breadcrumb-item">
            <a href="{{url('/blog')}}" class="link-hover-dark-primary">Blog</a>
          </li>
          <li class="breadcrumb-item"><span>{{$categorie->nom_categorie_blog}}</span></li>
        </ul>
      </div>
    </div>
  </div>

<div id="wrapper-content" class="wrapper-content">
    <div class="container">
        <div class="page-container row">
            <div class="page-content col-lg-8 mb-8 mb-lg-0">
                <div class="row post-style-3">
                    @foreach ($blogs as $blog)
                        <div class="col-md-6 mb-6">
                            <div class="card border-0">
                                <a href="{{route('guest.blog-index-item', [$blog->slug_categorie_blog, $blog->slug_article])}}" class="hover-scale">
                                    <img src="{{$blog->photo_article}}" alt="{{$blog->slug_categorie_blog}}"
                                        class="card-img-top image">
                                </a>
                                <div class="card-body px-0">
                                    <div class="mb-2">
                                        <a href="{{route('guest.blog-index', [$blog->slug_categorie_blog])}}" class="link-hover-dark-primary">{{$blog->nom_categorie_blog}}</a>
                                    </div>
                                    <h5 class="card-title lh-13 letter-spacing-25">
                                        <a href="{{route('guest.blog-index-item', [$blog->slug_categorie_blog, $blog->slug_article])}}"
                                            class="link-hover-dark-primary text-capitalize">
                                            {{$blog->titre_article}}
                                        </a>
                                    </h5>
                                    <ul class="list-inline">
                                        <li class="list-inline-item mr-0">
                                            <span class="text-gray">{{ \Carbon\Carbon::parse($blog->created_at)->translatedFormat('d F Y') }} par</span>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="#" class="link-hover-dark-primary">{{$blog->prenom_user}} {{$blog->nom_user}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <ul class="pagination pagination-style-01 mt-5">
                    {{$blogs->links()}}
                </ul>
            </div>
            <div class="sidebar col-lg-4 primary-sidebar sidebar-sticky" id="sidebar">
                <div class="primary-sidebar-inner sidebar-inner">
                    {{-- <div class="card border-0 mb-7 search">
                        <h5 class="card-title mb-6">Search</h5>
                        <div class="card-body p-0">
                            <form>
                                <label for="search" class="sr-only">Search</label>
                                <input type="text" id="search" class="form-control"
                                    placeholder="Type & Hit Enter...">
                            </form>
                        </div>
                    </div> --}}
                    <div class="card border-0 mb-6 category">
                        <h5 class="card-title mb-0">Categories</h5>
                        <div class="card-body px-0 bg-transparent">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item px-0 py-1">
                                    <a href="{{url('/blog')}}" class="link-hover-dark-primary">All</a>
                                </li>
                                @foreach ($categories as $categorie)
                                    <li class="list-group-item px-0 py-1">
                                        <a href="{{route('guest.blog-index', [$categorie->slug_categorie_blog])}}" class="link-hover-dark-primary">{{$categorie->nom_categorie_blog}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="card border-0 mb-7 recent-posts">
                        <h5 class="card-title mb-3">Recent Posts</h5>
                        <div class="card-body px-0 bg-transparent">
                            <ul class="list-group list-group-flush list-group-borderless">
                                @foreach ($newArticles as $articl)
                                    <li class="list-group-item bg-transparent p-0 mb-4">
                                        <a href="{{route('guest.blog-index-item', [$articl->slug_categorie_blog, $articl->slug_article])}}"
                                            class="font-size-md font-weight-semibold link-hover-dark-primary">
                                            {{$articl->titre_article}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="card rounded-0 border-0 bg-transparent">
                        <div class="datepicker-style-02" data-datepicker="true"
                            data-picker-option='{"inline":true,"language":"my-lang"}'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
