@extends('layouts.guest')

@section('content')
    <header id="header"
        class="main-header header-float header-sticky header-sticky-smart header-light header-style-03 font-normal">
        <div class="header-wrapper sticky-area">
            <div class="container">
                <nav class="navbar navbar-expand-xl">
                    <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                        <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                            <span></span>
                        </div>
                        <a class="navbar-brand navbar-brand-mobile" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true"
                            data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'>
                            <i class="far fa-search"></i>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse d-xl-flex" id="navbar-main-menu">
                        <a class="navbar-brand d-none d-xl-block" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/boutique-du-jour">Boutique du Jour <span class="caret"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="/categorie">Catégories <span class="caret"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/blog">Blog<span class="caret"></i></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/pricing">Offres<span class="caret"></i></span></a>
                            </li>
                            @auth
                                @if (Auth::user()->role_id == 1)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>

                                @elseif(Auth::user()->role_id == 2)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('home') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>

                                @endif
                            @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('login') }}"><i class="fas fa-user-circle mr-1"></i> Se
                                    connecter<span class="caret"></i></span></a>
                            </li>
                            @endauth
                            <li class="nav-item ">
                                <a class="btn btn-primary text-capitalize teest" href="{{ route('client.create-shop') }}"> +
                                    Ajoutez votre boutique<span class="caret"></i></span></a>
                            </li>
                        </ul>
                        <!-- {{-- <div class="header-customize justify-content-end align-items-center d-none d-xl-flex ml-auto"> --}}
                                {{-- <div class="header-customize-item">
                                    <a class="nav-link" href="/categorie">
                                        Categorie
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a class="nav-link" href="/blog">
                                        Catégories
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a class="nav-link" href="/pricing">
                                        Offres
                                    </a>
                                </div> --}}
                                {{-- <div class="header-customize-item">
                                    <a href="{{ url('login') }}" class="link">
                                        <svg class="icon icon-user-circle-o">
                                            <use xlink:href="#icon-user-circle-o"></use>
                                        </svg>
                                        Se connecter
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a href="{{ route('client.create-shop') }}" class="btn btn-primary text-capitalize">
                                        + Ajoutez votre boutique
                                    </a>
                                </div>
                            </div> --}} -->
                    </div>
                </nav>
            </div>
        </div>
    </header>

    <div class="content-wrap">
        <section class="banner index_head">
            <div class="container">
                <div class="banner-content">
                    <div class="heading" data-animate="fadeInLeft">
                        <h1 class="mb-0 text-white">
                            <span class="d-block lh-1 bleu bs">Boutique Sénégal</span>
                            <span class="d-block lh-1">1er Annuaire de Boutiques en ligne</span>
                        </h1>
                    </div>
                    <div class="form-search front-search" data-animate="fadeInRight">
                        <form action="{{route('guest.search')}}" method="GET">
                            <div class="row align-items-end">
                                <div class="col-8">
                                    <label for="key-word" class="text-white font-weight-bold text-uppercase">
                                        Que chercher vous ?</label>
                                    <div class="input-group rounded mr-5">
                                        <input type="text" id="key-word" name="search_query"
                                            class="form-control font-size-lg border-0 form-control-lg"
                                            placeholder="Ex: Chaussure, Robe, Bijou, Maquillage..."
                                            data-toggle="dropdown" aria-haspopup="true"
                                            autocomplete="off" />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <button type="submit"
                                        class="btn btn-primary-search font-weight-bold font-size-h5 btn-block btn-icon-left btn-lg lh-16">
                                        <i class="fal fa-search"><a class="cacher ">Chercher</a> </i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        </section>

        <section id="section-02" class="py-12 section_description section">
            <div class="container">
                <div class="row">
                    <div class="col-4" data-animate="fadeInUp">
                        <div class="media icon-box-style-02">
                            <div class="d-flex flex-column align-items-center mr-6 color-primary">
                               <i class="fas fa-store-alt icon-bs"></i><br>
                                {{-- <span class="number h1 font-weight-bold">1</span> --}}
                                <h4 class="mb-3 font-weight-bold text-center">
                                    Trouvez des boutiques à proximité
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-4" data-animate="fadeInUp">
                        <div class="media icon-box-style-02">
                            <div class="d-flex flex-column align-items-center mr-6 color-primary">
                               <i class="fas fa-store-alt icon-bs"></i><br>
                                {{-- <span class="number h1 font-weight-bold">1</span> --}}
                                <h4 class="mb-3 font-weight-bold text-center">
                                    Parcourir les avis de vrais clients
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-4" data-animate="fadeInUp">
                        <div class="media icon-box-style-02">
                            <div class="d-flex flex-column align-items-center mr-6 color-primary">
                               <i class="fas fa-store-alt icon-bs"></i><br>
                                {{-- <span class="number h1 font-weight-bold">1</span> --}}
                                <h4 class="mb-3 font-weight-bold text-center">
                                    Contactez les boutiques d'un click
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="section-03" class="pt-12 section_coup_coeur pb-13 bg-gray-03">
            <div class="container">
                <div class="d-flex align-items-center mb-8 flex-wrap flex-md-nowrap">
                    <h3 class="mb-0">Nos coups de <i class="fas fa-heart mr-1" style="color:#39d1ff"></i></h3>
                    <a href="{{ url('/categorie') }}"
                        class="link-hover-dark-primary ml-0 ml-md-auto w-100 w-md-auto mt-4 mt-md-0">
                        <span class="d-inline-block mr-2 font-size-md">Tout voir</span>
                        <i class="fal fa-chevron-right"></i>
                    </a>
                </div>
                <div class="slick-slider store-grid-style arrow-center"
                    data-slick-options='{"slidesToShow": 3, "autoplay":false,"dots":false,"responsive":[{"breakpoint": 1200,"settings": {"slidesToShow": 1,"arrows":false,"dots":true,"autoplay":true}},{"breakpoint": 992,"settings": {"slidesToShow": 2,"arrows":false,"dots":true,"autoplay":true}}]}'>
                    @foreach ($boutique_3 as $boutique)
                        @if ($boutique->pack_boutique_id == 3)
                            <div class="box" data-animate="slideInDown">
                                <div class="store card border-0 rounded-0 h-auto">
                                    <div class="position-relative store-image ">
                                        @if ($boutique->photo_boutique !== null)
                                            <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}">
                                                <img src="{{ $boutique->photo_boutique }}" alt="store 1"
                                                    class="card-img-top card-image rounded-0 " />
                                            </a>
                                        @else
                                            <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}">
                                                <img src="{{asset('images/bs.png')}}" alt="store 1"
                                                    class="card-img-top card-image rounded-0" />
                                            </a>
                                        @endif

                                        <div class="image-content position-absolute d-flex align-items-center">
                                            <div class="content-right ml-auto d-flex">
                                                <a @if ($boutique->photo_boutique !== null)
                                                    href="{{ $boutique->photo_boutique }}"
                                                @else
                                                    href="{{asset('images/bs.png')}}"
                                                @endif style="width:100px; height:100px; " class="item viewing"
                                                    data-toggle="tooltip" data-placement="top" title="Quickview"
                                                    data-gtf-mfp="true">
                                                    <svg class="icon icon-expand">
                                                        <use xlink:href="#icon-expand"></use>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}"
                                            class="card-title h5 text-dark d-inline-block mb-2">
                                            <span class="letter-spacing-25 text-uppercase">{{ $boutique->nom_boutique }}</span>
                                        </a>
                                        <ul
                                            class="list-inline store-meta mb-4 font-size-sm d-flex align-items-center flex-wrap none-display cache">

                                            <li class="list-inline-item">
                                                <span class="badge badge-success d-inline-block mr-1">8</span>
                                                <span>8 notes</span>
                                            </li>
                                            <li class="list-inline-item separate"></li>
                                            <li class="list-inline-item">
                                                <span class="mr-1">Catégorie </span>
                                                <span class="text-danger font-weight-semibold">{{ $boutique->nom_categorie_boutique }}</span>
                                            </li>

                                            <li class="list-inline-item separate"></li>
                                            <li class="list-inline-item mt-1">
                                                <span class="text-green">Ouvert !</span>
                                            </li>
                                        </ul>
                                        <div class="card-footer rounded-0 border-top pt-3 bg-transparent px-0 pb-0 mt-3 cache">
                                            <span class="d-inline-block mr-1">
                                                <i class="fal fa-map-marker-alt"> </i>
                                            </span>
                                            <a href="#"
                                                class="text-secondary text-decoration-none address">{{ $boutique->adresse_boutique }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>

        <section id="section-05" class="pt-12 pb-13 section_categotie bg-pattern-01">
            <div class="container">
                <div class="text-center mb-8">
                    <h3 class="bleu mb-0">Nos Catégories</h3>
                </div>
                <div class="row align-items-center justify-content-center">
                    @foreach ($categories as $categorie)
                        <div class="col-md-4 col-sm-4 section_categorie_container mb-6">
                            <div class="image-box section_categorie_container_box card mb-6 rounded-0 border-0 hover-scale" data-animate="zoomIn">
                                @if ($categorie->photo_categorie_boutique == null)
                                    <a href="{{ route('guest.categorie-index', [$categorie->slug_categorie_boutique]) }}" class="image position-relative card-img">
                                        <img src="{{asset('images/listing/categorie.jpg')}}" alt="Catégorie Image" class="section_categorie_img" />
                                    </a>
                                @else
                                    <a href="{{ route('guest.categorie-index', [$categorie->slug_categorie_boutique]) }}" class="image position-relative card-img">
                                        <img src="{{$categorie->photo_categorie_boutique}}" alt="Catégorie Image" class="section_categorie_img"/>
                                    </a>
                                @endif
                                <div class="text-white content-box px-4 pb-3 card-img-overlay cache">
                                    @if ($categorie->boutiques_count == 0)
                                            <p class="mb-1 bleu">{{$categorie->boutiques_count}} Boutique</p>
                                    @else
                                            <p class="mb-1 bleu">{{$categorie->boutiques_count}} Boutiques</p>
                                    @endif
                                    <a href="{{ route('guest.categorie-index', [$categorie->slug_categorie_boutique]) }}" class="font-weight-normal text-white font-size-lg">
                                        {{$categorie->nom_categorie_boutique}}
                                    </a>
                                </div>
                            </div>
                        </div>
                     @endforeach
                </div>
            </div>
        </section>

        <section id="section-03" class="pt-12 pb-13 section_boutique_recent bg-gray-03">
            <div class="container">
                <div class="d-flex align-items-center mb-8 flex-wrap flex-md-nowrap">
                    <h3 class="mb-0 ">Boutiques récentes</h3>
                </div>
                <div class="slick-slider boutique_recent store-grid-style arrow-center"
                    data-slick-options='{"slidesToShow": 3, "autoplay":false,"dots":false,"responsive":[{"breakpoint": 1200,"settings": {"slidesToShow": 1,"arrows":false,"dots":true,"autoplay":true}},{"breakpoint": 992,"settings": {"slidesToShow": 2,"arrows":false,"dots":true,"autoplay":true}}]}'>
                    @foreach ($boutique_r as $boutique)
                        <div class="box" data-animate="slideInDown">
                            <div class="store card border-0 rounded-0 h-auto">
                                <div class="position-relative store-image">
                                    <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}">
                                    @if ($boutique->photo_boutique !== null)
                                        <img src="{{ $boutique->photo_boutique }}" alt="store 1"
                                            class="card-img-top card-image rounded-0" />
                                    @else

                                    <img src="{{asset('images/bs.png')}}" alt="store 1"
                                            class="card-img-top card-image rounded-0" />
                                    @endif
                                    </a>
                                    <div class="image-content position-absolute d-flex align-items-center">
                                        <div class="content-right ml-auto d-flex">
                                        @if ($boutique->photo_boutique !== null)
                                            <a href="{{ $boutique->photo_boutique }}" class="item viewing"
                                                data-toggle="tooltip" data-placement="top" title="Quickview"
                                                data-gtf-mfp="true">
                                                <svg class="icon icon-expand">
                                                    <use xlink:href="#icon-expand"></use>
                                                </svg>
                                            </a>
                                        @else
                                            <a href="{{asset('images/bs.png')}}" class="item viewing"
                                                data-toggle="tooltip" data-placement="top" title="Quickview"
                                                data-gtf-mfp="true">
                                                <svg class="icon icon-expand">
                                                    <use xlink:href="#icon-expand"></use>
                                                </svg>
                                            </a>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}"
                                        class="card-title h5 text-dark d-inline-block mb-2">
                                        <span class="letter-spacing-25 text-uppercase">{{ $boutique->nom_boutique }}</span>
                                    </a>
                                    <ul
                                        class="list-inline store-meta mb-4 font-size-sm d-flex align-items-center flex-wrap mt-1 none-display cache">
                                        <li class="list-inline-item">
                                            <span class="mr-1">Categorie: </span>
                                            <span class="text-danger font-weight-semibold">{{$boutique->categorie->nom_categorie_boutique}}</span>
                                        </li>
                                        <br>
                                        <li class="list-inline-item separate"></li>
                                        <li class="list-inline-item mt-1">
                                            <span class="text-green">Ouvert !</span>
                                        </li>
                                    </ul>
                                    <div class="card-footer rounded-0 border-top pt-3 bg-transparent px-0 pb-0 mt-3 cache">
                                        <span class="d-inline-block mr-1">
                                            <i class="fal fa-map-marker-alt"> </i>
                                        </span>
                                        <a href="#"
                                            class="text-secondary text-decoration-none address">{{ $boutique->adresse_boutique }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

        <section id="section-07" class="pb-9 section_blog post-style-4 cache">
            <div class="container">
                <div class="border-top"></div>
                <div class="text-center mb-8 section_blog_text pt-11">
                    <h3 class="text-capitalize bleu">Notre Blog</h3>
                </div>
                <div class="row">
                    @foreach ($articles as $article)
                    <div class="col-md-4 mb-4 shadow" data-animate="zoomIn">
                            <div class="card border-0">
                                <a href="/blog" class="hover-scale border">
                                    <img src="{{$article->photo_article}}" alt="product 1" class="card-img-top card-image image" />
                                </a>
                                <div class="card-body px-0">
                                    <ul class="list-inline mb-2">
                                        <li class="list-inline-item mr-0">
                                            <span class="text-gray">{{\Carbon\Carbon::parse($article->created_at)->translatedFormat('d F Y')}}</span>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="#" class="link-hover-dark-primary">{{$article->prenom_user}}</a>
                                        </li>
                                    </ul>
                                    <h5 class="card-title font-size-lg text-capitalize lh-14 letter-spacing-25">
                                        <a href="/blog" class="link-hover-dark-primary">{{$article->titre_article}}</a>
                                    </h5>
                                </div>
                            </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>

        <section id="section-08" class="bg-primary section_do_you py-7">
            <div class="container">
                <div class="row align-items-center">
                    <div class="text-white col-lg-9">
                        <h2 class="mb-1 section_do_you_h2 text-white">
                            Avez-vous une boutique en ligne ?
                        </h2>
                        <p class="mb-0 font-size-md">
                            Découvrer comment Boutique Senegal peut vous aider
                        </p>
                    </div>
                    <div class="col-lg-3 text-left text-lg-right mt-4 mt-lg-0">
                        <a href="#" class="btn btn-outline-white btn-icon-right font-size-h5 px-6 py-3 lh-1">
                        En savoir plus
                            <i class="fal fa-chevron-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>
@section('script')
    <script>
        $('.header-mobile').on('click', '.search-toggle', function(e) {
            console.log('click');
            var selector = $(this).data('selector');

            $(selector).toggleClass('show').find('.search-input').focus();
            $(this).toggleClass('active');

            e.preventDefault();
        });
    </script>
@endsection
@endsection
