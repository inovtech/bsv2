@extends('layouts.app')
@section('content')
<header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
    <div class="header-wrapper sticky-area border-bottom">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true" data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'><i class="far fa-search"></i></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>

<div id="wrapper-content" class="wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
        @include('Admin.Layouts.sidebar')

        <div class="page-container">
            <div class="container-fluid h-100">
                <div class="page-content-wrapper d-flex flex-column">
                    <h1 class="font-size-h4 mb-4 font-weight-normal">Utilisateur</h1>
                    <div class="page-content">
                        <div class="tabs">
                            <ul class="nav nav-pills tab-style-01 font-size-lg" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all"
                                        role="tab" aria-controls="all" aria-selected="true">Tous les utilisateurs ({{$coun_all->count()}})
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="active-tab" data-toggle="tab" href="#active"
                                        role="tab" aria-controls="active" aria-selected="false">Liste administrateurs
                                        ({{$coun_ad->count()}}) </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pending-tab" data-toggle="tab" href="#pending"
                                        role="tab" aria-controls="pending" aria-selected="false"> Liste clients
                                        Listings
                                        ({{$coun_cl->count()}}) </a>
                                </li>
                                <li class="nav-item">
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                        Nouveau <i class="mdi mdi-chevron-down"></i>
                                    </button>
                                    <ul class="dropdown-menu" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(12px, 37px, 0px);" x-placement="bottom-start">
                                        <li><a href="{{url('/admin/nouveau-admin/create')}}" class="dropdown-item">ADMIN</a></li>
                                        <li><a href="{{url('/admin/nouveau-client/create')}}" class="dropdown-item">CLIENT</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="all" role="tabpanel"
                                aria-labelledby="all-tab">
                                <div class="store-listing-style-04">
                                    @foreach ($users as $user)
                                        <div class="store-listing-item">
                                            <div
                                                class="d-flex align-items-center flex-wrap flex-lg-nowrap border-bottom py-4 py-lg-0">
                                                <div class="store media align-items-stretch py-4">
                                                    <a href="#" class="store-image">
                                                        @if ($user->profile_photo_path == null)
                                                            <img src="{{asset('images/bs.jpeg')}}" alt="Boutique-Senegal">
                                                        @else
                                                            <img src="{{asset($user->profile_photo_path)}}" alt="{{$user->nom_user}}">
                                                        @endif
                                                    </a>
                                                    <div class="media-body px-0 pt-4 pt-md-0">
                                                        <a href="#"
                                                            class="font-size-lg font-weight-semibold text-dark d-inline-block mb-2 lh-1"><span
                                                                class="letter-spacing-25">{{$user->prenom_user}} {{$user->nom_user}}</span>
                                                        </a>
                                                        <ul
                                                            class="list-inline store-meta mb-3 font-size-sm d-flex align-items-center flex-wrap">
                                                            <li class="list-inline-item">
                                                                <img src="https://img.icons8.com/clouds/20/000000/email.png"/>
                                                                    <span class="number">{{$user->email}}</span>
                                                            </li>
                                                            <li class="list-inline-item separate"></li>
                                                            <li class="list-inline-item"><a href="#"
                                                                    class="link-hover-secondary-primary">
                                                                    <img src="https://img.icons8.com/doodle/20/000000/whatsapp.png"/>
                                                                    <span>{{$user->telephone_user}}</span>
                                                                </a></li>
                                                        </ul>
                                                        <div class="border-top pt-2 d-flex">
                                                            <span class="d-inline-block mr-1"><i
                                                                    class="fal fa-map-marker-alt">
                                                                </i>
                                                            </span>
                                                            <a href="#"
                                                                class="text-secondary text-decoration-none address">
                                                                {{$user->adresse_user}}
                                                            </a>
                                                            <div class="ml-0 ml-sm-auto">
                                                                <span class="label">Status:</span>
                                                                @if ($user->statut_user == 1)
                                                                    <span class="status active">Active</span>
                                                                @else
                                                                    <span class="status text-danger">Desactive</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    class="action ml-0 ml-lg-auto mt-3 mt-lg-0 align-items-center flex-wrap flex-sm-nowrap w-100 w-lg-auto">
                                                    <a @if ($user->role_id == 1)  href="{{route('utilisateur.edit-a', [$user->id])}}" @else href="{{route('utilisateur.edit-c', [$user->id])}}" @endif
                                                        class="btn btn-light btn-icon-left mb-2 mb-sm-0 font-size-md">
                                                        <i class="fal fa-edit"></i>
                                                        Edit
                                                    </a>
                                                    <a href=""
                                                        class="btn btn-primary btn-icon-left mb-2 mb-sm-0 px-5 font-size-md" data-toggle="modal" onclick="deleteData( {{ $user->id }} )" data-target="#modalDeletePack">
                                                        <i class="fal fa-trash-alt"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <ul class="pagination pagination-style-02">
                                    {{$users->links()}}
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="active" role="tabpanel" aria-labelledby="active-tab">
                                <div class="store-listing-style-04">
                                    @foreach ($admins as $admin)
                                        <div class="store-listing-item">
                                            <div
                                                class="d-flex align-items-center flex-wrap flex-lg-nowrap border-bottom py-4 py-lg-0">
                                                <div class="store media align-items-stretch py-4">
                                                    <a href="#" class="store-image">
                                                        @if ($admin->profile_photo_path == null)
                                                            <img src="{{asset('images/bs.jpeg')}}" alt="Boutique-Senegal">
                                                        @else
                                                            <img src="{{asset($admin->profile_photo_path)}}" alt="{{$admin->nom_user}}">
                                                        @endif
                                                    </a>
                                                    <div class="media-body px-0 pt-4 pt-md-0">
                                                        <a href="#"
                                                            class="font-size-lg font-weight-semibold text-dark d-inline-block mb-2 lh-1"><span
                                                                class="letter-spacing-25">{{$admin->prenom_user}} {{$admin->nom_user}}</span>
                                                        </a>
                                                        <ul
                                                            class="list-inline store-meta mb-3 font-size-sm d-flex align-items-center flex-wrap">
                                                            <li class="list-inline-item">
                                                                <img src="https://img.icons8.com/clouds/20/000000/email.png"/>
                                                                    <span class="number">{{$admin->email}}</span>
                                                            </li>
                                                            <li class="list-inline-item separate"></li>
                                                            <li class="list-inline-item"><a href="#"
                                                                    class="link-hover-secondary-primary">
                                                                    <img src="https://img.icons8.com/doodle/20/000000/whatsapp.png"/>
                                                                    <span>{{$admin->telephone_user}}</span>
                                                                </a></li>
                                                        </ul>
                                                        <div class="border-top pt-2 d-flex">
                                                            <span class="d-inline-block mr-1"><i
                                                                    class="fal fa-map-marker-alt">
                                                                </i>
                                                            </span>
                                                            <a href="#"
                                                                class="text-secondary text-decoration-none address">
                                                                {{$admin->adresse_user}}
                                                            </a>
                                                            <div class="ml-0 ml-sm-auto">
                                                                <span class="label">Status:</span>
                                                                @if ($admin->statut_user == 1)
                                                                    <span class="status active">Active</span>
                                                                @else
                                                                    <span class="status text-danger">Desactive</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    class="action ml-0 ml-lg-auto mt-3 mt-lg-0 align-items-center flex-wrap flex-sm-nowrap w-100 w-lg-auto">
                                                    <a href="{{route('utilisateur.edit-a', [$admin->id])}}"
                                                        class="btn btn-light btn-icon-left mb-2 mb-sm-0 font-size-md">
                                                        <i class="fal fa-edit"></i>
                                                        Edit
                                                    </a>
                                                    <a href="#"
                                                        class="btn btn-primary btn-icon-left mb-2 mb-sm-0 px-5 font-size-md" data-toggle="modal" onclick="deleteData( {{ $admin->id }} )" data-target="#modalDeletePack">
                                                        <i class="fal fa-trash-alt"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <ul class="pagination pagination-style-02">
                                    {{$admins->links()}}
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="pending" role="tabpanel"
                                aria-labelledby="pending-tab">
                                <div class="store-listing-style-04">
                                    @foreach ($clients as $client)
                                        <div class="store-listing-item">
                                            <div
                                                class="d-flex align-items-center flex-wrap flex-lg-nowrap border-bottom py-4 py-lg-0">
                                                <div class="store media align-items-stretch py-4">
                                                    <a href="#" class="store-image">
                                                        @if ($client->profile_photo_path == null)
                                                            <img src="{{asset('images/bs.jpeg')}}" alt="Boutique-Senegal">
                                                        @else
                                                            <img src="{{asset($client->profile_photo_path)}}" alt="{{$client->nom_user}}">
                                                        @endif
                                                    </a>
                                                    <div class="media-body px-0 pt-4 pt-md-0">
                                                        <a href="#"
                                                            class="font-size-lg font-weight-semibold text-dark d-inline-block mb-2 lh-1"><span
                                                                class="letter-spacing-25">{{$client->prenom_user}} {{$client->nom_user}}</span>
                                                        </a>
                                                        <ul
                                                            class="list-inline store-meta mb-3 font-size-sm d-flex align-items-center flex-wrap">
                                                            <li class="list-inline-item">
                                                                <img src="https://img.icons8.com/clouds/20/000000/email.png"/>
                                                                    <span class="number">{{$client->email}}</span>
                                                            </li>
                                                            <li class="list-inline-item separate"></li>
                                                            <li class="list-inline-item"><a href="#"
                                                                    class="link-hover-secondary-primary">
                                                                    <img src="https://img.icons8.com/doodle/20/000000/whatsapp.png"/>
                                                                    <span>{{$client->telephone_user}}</span>
                                                                </a></li>
                                                        </ul>
                                                        <div class="border-top pt-2 d-flex">
                                                            <span class="d-inline-block mr-1"><i
                                                                    class="fal fa-map-marker-alt">
                                                                </i>
                                                            </span>
                                                            <a href="#"
                                                                class="text-secondary text-decoration-none address">
                                                                {{$client->adresse_user}}
                                                            </a>
                                                            <div class="ml-0 ml-sm-auto">
                                                                <span class="label">Status:</span>
                                                                @if ($client->statut_user == 1)
                                                                    <span class="status active">Active</span>
                                                                @else
                                                                    <span class="status text-danger">Desactive</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    class="action ml-0 ml-lg-auto mt-3 mt-lg-0 align-items-center flex-wrap flex-sm-nowrap w-100 w-lg-auto">
                                                    <a href="{{route('utilisateur.edit-c', [$client->id])}}"
                                                        class="btn btn-light btn-icon-left mb-2 mb-sm-0 font-size-md">
                                                        <i class="fal fa-edit"></i>
                                                        Edit
                                                    </a>
                                                    <a href="" class="btn btn-primary btn-icon-left mb-2 mb-sm-0 px-5 font-size-md" data-toggle="modal" onclick="deleteData( {{ $client->id }} )" data-target="#modalDeletePack">
                                                        <i class="fal fa-trash-alt"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <ul class="pagination pagination-style-02">
                                    {{$clients->links()}}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="mt-5">
                        &copy; 2020 Thedir. All Rights Reserved.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
    <div class="modal fade" id="modalDeletePack" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header bg-light">
                    <h5 class="text-uppercase" id="exampleModalLongTitle"><i class="mdi mdi-account-circle mr-1"></i>
                            Suppression Utilisateur
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="deleteForm" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                <div class="modal-body">
                        <p class="text-center" style="font-size:16px">Etes-vous sûr de vouloir supprimer cet utilisateur ?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger mr-1" data-dismiss="modal" onclick="formSubmit()">Supprimer</button>
                    <button type="close" class="btn btn-success" data-dismiss="modal" >Annuler</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @section('script')
        <script type=text/javascript>
            function deleteData(id)
            {
                var id = id;
                var url = '{{ route("utilisateur.destroy", ":id") }}';
                url = url.replace(':id', id);
                $("#deleteForm").attr('action', url);
            }

            function formSubmit()
            {
                $("#deleteForm").submit();
            }
        </script>
    @endsection

@endsection
