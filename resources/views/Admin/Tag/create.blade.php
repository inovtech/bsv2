@extends('layouts.app')
@section('content')
<header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
    <div class="header-wrapper sticky-area border-bottom">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="bs">
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true" data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'><i class="far fa-search"></i></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="bs">
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>

<div id="wrapper-content" class="wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
        @include('Admin.Layouts.sidebar')

        <div class="page-container">
            <div class="container-fluid h-100">
                <div class="page-content-wrapper d-flex flex-column h-100">
                    <h1 class="font-size-h4 mb-4 font-weight-normal">Tag Blog</h1>
                    <div class="page-content">
                        <div class="card rounded-0 border-0 bg-white px-4 pt-3 pb-6">
                            <div class="card-header p-0 bg-transparent">
                                <h5 class="card-title text-capitalize">Nouveau Tag Blog</h5>
                            </div>
                            <div class="card-body px-0 pt-4 pb-0">
                                <div class="form-update-profile">
                                    <form method="POST" action="{{ route('tag.store') }}">
                                        @csrf
                                        <div class="form-group mb-2">
                                            <label for="libelle" class="font-size-md text-dark font-weight-semibold mb-1">Libelle <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control @error('libelle') border border-danger @enderror" id="libelle" name="libelle" placeholder="Libelle">
                                            @error('libelle')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="description" class="font-size-md text-dark font-weight-semibold mb-1">Description</label>
                                            <input type="textarea" class="form-control" id="description" name="description" placeholder="Description">
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block font-size-lg">AJOUTER</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-auto pt-6">&copy; 2020 Thedir. All Rights Reserved.</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
