<div class="sidebar">
    <div class="container-fluid">
        <div class="user-profile media align-items-center mb-6">
            <div class="image mr-3">
                @if (Auth::user()->profile_photo_path == null)
                    <a href="{{url('/admin/profil')}}">
                        <img src="{{asset('images/bs.jpeg')}}" alt="Boutique-Senegal" class="rounded-circle">
                    </a>
                @else
                    <a href="{{url('/admin/profil')}}">
                        <img src="{{asset(Auth::user()->profile_photo_path)}}" alt="{{Auth::user()->nom_user}}">
                    </a>
                @endif
            </div>
                <div class="media-body lh-14">
                <span class="text-dark d-block font-size-md">Salam,</span>
                <span class="mb-0 h5">{{ Auth::user()->prenom_user }} {{ Auth::user()->nom_user }}!</span>
            </div>
        </div>
        <ul class="list-group list-group-flush list-group-borderless">
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="{{url("/admin/dashboard")}}" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                    <span class="d-inline-block mr-3"><i class="fal fa-tachometer"></i></span>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="#listing" class="d-flex align-items-center link-hover-dark-primary font-size-md" data-toggle="collapse" aria-expanded="false">
                    <span class="d-inline-block mr-3"><i class="fal fa-store"></i></span>
                    <span>Boutique</span>
                    <span class=" ml-auto"><i class="fal fa-chevron-down"></i></span>
                </a>
                <ul class="submenu collapse list-group list-group-flush list-group-borderless pt-2 mb-0 sidebar-menu" id="listing">
                    <li class="list-group-item p-0 mb-2 lh-15">
                        <a href="{{url("/admin/boutique")}}" class="link-hover-dark-primary font-size-md">Liste</a>
                    </li>
                    <li class="list-group-item p-0 mb-2 lh-15">
                        <a href="{{url("/admin/boutique/create")}}" class="link-hover-dark-primary font-size-md">Nouvelle</a>
                    </li>
                </ul>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="{{url("/admin/produits")}}" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                    <span class="d-inline-block mr-3"><i class="fal fa-shopping-bag"></i></span>
                    <span>Produit</span>
                </a>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="#cat" class="d-flex align-items-center link-hover-dark-primary font-size-md" data-toggle="collapse" aria-expanded="false">
                    <span class="d-inline-block mr-3"><i class="fal fa-layer-group"></i></span>
                    <span>Catégories</span>
                    <span class=" ml-auto"><i class="fal fa-chevron-down"></i></span>
                </a>
                <ul class="submenu collapse list-group list-group-flush list-group-borderless pt-2 mb-0 sidebar-menu" id="cat">
                    <li class="list-group-item p-0 mb-2 lh-15">
                        <a href="{{url('/admin/categorie-boutique')}}" class="link-hover-dark-primary font-size-md">
                            <span>Categorie Boutique</span>
                        </a>
                    </li>
                    <li class="list-group-item p-0 mb-2 lh-15">
                        <a href="{{url('/admin/sous-categorie-boutique')}}" class="link-hover-dark-primary font-size-md">
                            <span>Sous Categorie Boutique</span>
                        </a>
                    </li>
                    <li class="list-group-item p-0 mb-2 lh-15">
                        <a href="{{url('/admin/categorie-produit')}}" class="link-hover-dark-primary font-size-md">
                            <span>Categorie Produit</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="{{url('/admin/utilisateur')}}" class="link-hover-dark-primary font-size-md">
                    <span class="ml-auto mr-1"><i class="fal fa-users"></i></span>
                    <span>Utilisateurs</span>
                </a>
            </li>
            <!-- <li class="list-group-item p-0 mb-2 lh-15">
                <a href="panel-my-favourite.html" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                    <span class="d-inline-block mr-3"><i class="fal fa-bookmark"></i></span>
                    <span>Saved</span>
                </a>
            </li> -->
            {{-- <li class="list-group-item p-0 mb-2 lh-15">
                <a href="#invoice" class="d-flex align-items-center link-hover-dark-primary font-size-md" data-toggle="collapse" aria-expanded="false">
                    <span class="d-inline-block mr-3"><svg class="icon icon-receipt"><use xlink:href="#icon-receipt"></use></svg></span>
                    <span>Message</span>
                    <span class=" ml-auto"><i class="fal fa-chevron-down"></i></span>
                </a>
                <ul class="submenu collapse list-group list-group-flush list-group-borderless pt-2 mb-0 sidebar-menu" id="invoice">
                    <li class="list-group-item p-0 mb-2 lh-15"><a href="panel-invoice-listing.html" class="link-hover-dark-primary font-size-md">
                    Nouveau message</a></li>
                    <li class="list-group-item p-0 mb-2 lh-15"><a href="panel-invoice-details.html" class="link-hover-dark-primary font-size-md">
                    Boite de reception</a></li>
                </ul>
            </li> --}}
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="#adcampaign" class="d-flex align-items-center link-hover-dark-primary font-size-md" data-toggle="collapse" aria-expanded="false">
                    <span class="d-inline-block mr-3"><i class="far fa-bullhorn"></i></span>
                    <span>Blog</span>
                    <span class=" ml-auto"><i class="fal fa-chevron-down"></i></span>
                </a>
                <ul class="submenu collapse list-group list-group-flush list-group-borderless pt-2 mb-0 sidebar-menu" id="adcampaign">
                    <li class="list-group-item p-0 mb-2 lh-15"><a href="{{url('/admin/article/create')}}" class="link-hover-dark-primary font-size-md">
                    Ajout Article</a></li>
                    <li class="list-group-item p-0 mb-2 lh-15"><a href="{{url('/admin/article')}}" class="link-hover-dark-primary font-size-md">
                    Liste Articles</a></li>
                    <li class="list-group-item p-0 mb-2 lh-15"><a href="{{url('/admin/categorie-blog')}}" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                    Categorie Blog</a></li>
                </ul>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="{{url('/admin/profil')}}" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                    <span class="d-inline-block mr-3"><svg class="icon icon-user"><use xlink:href="#icon-user"></use></svg></span>
                    <span>Mon Profile</span>
                </a>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="#parametre" class="d-flex align-items-center link-hover-dark-primary font-size-md" data-toggle="collapse" aria-expanded="false">
                    <span class="d-inline-block mr-3"><svg class="icon icon-cog"><use xlink:href="#icon-cog"></use></svg></span>
                    <span>Paramètres</span>
                    <span class=" ml-auto"><i class="fal fa-chevron-down"></i></span>
                </a>
                <ul class="submenu collapse list-group list-group-flush list-group-borderless pt-2 mb-0 sidebar-menu" id="parametre">
                    <li class="list-group-item p-0 mb-2 lh-15">
                        <a href="{{url('/admin/pack-abonnement')}}" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                            <span>Pack</span>
                        </a>
                    </li>
                    <li class="list-group-item p-0 mb-2 lh-15">
                        <a href="{{url('/admin/tag')}}" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                            <span>Tag</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                    <span class="d-inline-block mr-3"><svg class="icon icon-exit"><use xlink:href="#icon-exit"></use></svg></span>
                    <span>Logout</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</div>
