<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<!--begin::Head-->
	<head>
		<meta charset="utf-8" />
		<title>Boutique Senegal | Connexion</title>
		<meta name="description" content="Login page example" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="canonical" href="https://keenthemes.com/metronic" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Custom Styles(used by this page)-->
		<link href="{{asset('loggin/assets/css/pages/login/classic/login-6.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Page Custom Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="{{asset('loggin/assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('loggin/assets/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('loggin/assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<link href="{{asset('loggin/assets/css/themes/layout/header/base/light.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('loggin/assets/css/themes/layout/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Layout Themes-->
        <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Login-->
			<div class="login login-6 login-signin-on login-signin-on d-flex flex-column-fluid" id="kt_login">
				<div class="d-flex flex-column flex-lg-row flex-row-fluid text-center" style="background-image: url({{asset('loggin/assets/media/bg/bg-3.jpg);')}}">
					<!--begin:Aside-->
					<div class="d-flex w-100 flex-center p-15">
						<div class="login-wrapper">
							<!--begin:Aside Content-->
							<div class="text-dark-75">
								<a href="/">
									<img src="{{asset('images/bs-login.png')}}" class="max-h-75px" alt="" />
								</a>
								<h3 class="mb-8 mt-22 font-weight-bold">REJOINDRE NOTRE COMMUNAUTE</h3>
								<p class="mb-15 text-muted font-weight-bold">Boutique Senegal, le 1er annuaire de boutiques en ligne.</p>

								<button type="button" id="kt_back_to_website" class="btn btn-primary btn-pill font-weight-bold px-9 py-4 my-3 mx-2">Revenir sur le site</button>
								<button type="button" id="kt_login_signup" class="btn btn-outline-primary btn-pill py-4 px-9 font-weight-bold">Ouvrir un compte</button>
							</div>
							<!--end:Aside Content-->
						</div>
					</div>
					<!--end:Aside-->
					<!--begin:Divider-->
					<div class="login-divider">
						<div></div>
					</div>
					<!--end:Divider-->
					<!--begin:Content-->
					<div class="d-flex w-100 flex-center p-15 position-relative overflow-hidden">
						<div class="login-wrapper">
							<!--begin:Sign In Form-->
							
							<div class="login-signin">
								<div class="text-center mb-10 mb-lg-20">
									<h2 class="font-weight-bold">LOGIN</h2>
									<p class="text-muted font-weight-bold">Veuillez entrer votre email et mot de passe</p>
								</div>
								<form class="form text-left" id="kt_login_signin_form" method="POST" action="{{ route('login') }}">
                                    @csrf
									<div class="form-group py-2 m-0">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75 @error('email') is-invalid @enderror" type="email" placeholder="Email" name="email" autocomplete="off" value="{{ old('email') }}" />
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="form-group py-2 border-top m-0">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75 @error('password') is-invalid @enderror" type="Password" placeholder="Mot de Passe" name="password" />
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="form-group d-flex flex-wrap justify-content-between align-items-center mt-5">
										<div class="checkbox-inline">
											<label class="checkbox m-0 text-muted font-weight-bold">
											<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
											<span></span>Se souvenir de moi</label>
										</div>

                                        @if (Route::has('password.request'))
                                            <a href="{{ route('password.request') }}" id="kt_login_forgot" class="text-muted text-hover-primary font-weight-bold">Mot de Passe oublié ?</a>
                                        @endif
									</div>
									<div class="text-center mt-15">
										<button id="kt_login_signin_submit" type="submit" class="btn btn-primary btn-pill shadow-sm py-4 px-9 font-weight-bold">Se connecter</button>
									</div>
								</form>
							</div>
							<!--end:Sign In Form-->
							<!--begin:Sign Up Form-->
							<div class="login-signup">
								<div class="text-center mb-10 mb-lg-20">
									<h3 class="">Inscription</h3>
									<p class="text-muted font-weight-bold">Veuillez renseigner ces champs pour créer un compte</p>
								</div>
								<form method="POST" class="form text-left" id="kt_login_signup_form" action="{{ route('register') }}">
                                    @csrf
									<div class="form-group py-2 m-0">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75 @error('prenom_user') is-invalid @enderror" type="text" placeholder="Prénom" name="prenom_user" />
                                        @error('prenom_user')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="form-group py-2 m-0">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75 @error('nom_user') is-invalid @enderror" type="text" placeholder="Nom" name="nom_user" />
                                        @error('nom_user')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="form-group py-2 m-0 border-top">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75 @error('email') is-invalid @enderror" type="email" placeholder="Adresse Email" name="email" autocomplete="off" />
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="form-group py-2 m-0 border-top">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75 @error('telephone_user') is-invalid @enderror" type="text" placeholder="Numéro de Téléphone" name="telephone_user"/>
                                        @error('telephone_user')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="form-group py-2 m-0 border-top">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75 @error('adresse_user') is-invalid @enderror" type="text" placeholder="Adresse" name="adresse_user"/>
                                        @error('adresse_user')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="form-group py-2 m-0 border-top">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75 @error('password') is-invalid @enderror" type="password" placeholder="Mot de passe" name="password" />
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="form-group py-2 m-0 border-top">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75 @error('password_confirmation') is-invalid @enderror" type="password" placeholder="Confirmer le mot de passe" name="password_confirmation" />
                                        @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
									<div class="form-group d-flex flex-wrap flex-center">
										<button id="kt_login_signup_submit" type="submit" class="btn btn-primary btn-pill font-weight-bold px-9 py-4 my-3 mx-2">S'inscrire</button>
										<button id="kt_login_signup_cancel" class="btn btn-outline-primary btn-pill font-weight-bold px-9 py-4 my-3 mx-2">Annuler</button>
									</div>
								</form>
							</div>
							<!--end:Sign Up Form-->
							<!--begin:Forgot Password Form-->
							<div class="login-forgot">
								<div class="text-center mb-10 mb-lg-20">
									<h3 class="">Mot de Passe oublié ?</h3>
									<p class="text-muted font-weight-bold">Veuillez entrer votre email pour réinitialiser le mot de passe</p>
								</div>
								<form class="form text-left" id="kt_login_forgot_form" method="POST" action="{{ route('password.email') }}">
                                    @csrf
									<div class="form-group py-2 m-0 border-bottom">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75" type="text" placeholder="Email" name="email" autocomplete="off" />
									</div>
									<div class="form-group d-flex flex-wrap flex-center mt-10">
										<button id="kt_login_forgot_submit" type="submit" class="btn btn-primary btn-pill font-weight-bold px-9 py-4 my-3 mx-2">Envoyer</button>
										<button id="kt_login_forgot_cancel" class="btn btn-outline-primary btn-pill font-weight-bold px-9 py-4 my-3 mx-2">Annuler</button>
									</div>
								</form>
							</div>
							<!--end:Forgot Password Form-->
						</div>
					</div>
					<!--end:Content-->
				</div>
			</div>
			<!--end::Login-->
		</div>
		<!--end::Main-->
		<!--begin::Global Config(global config for global JS scripts)-->
		<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
		<!--end::Global Config-->
		<!--begin::Global Theme Bundle(used by all pages)-->
		<script src="{{asset('loggin/assets/plugins/global/plugins.bundle.js')}}"></script>
		<script src="{{asset('loggin/assets/plugins/custom/prismjs/prismjs.bundle.js')}}"></script>
		<script src="{{asset('loggin/assets/js/scripts.bundle.js')}}"></script>
		<!--end::Global Theme Bundle-->
		<!--begin::Page Scripts(used by this page)-->
		<script src="{{asset('loggin/assets/js/pages/custom/login/login-general.js')}}"></script>
		<!--end::Page Scripts-->
	</body>
	<!--end::Body-->
</html>
