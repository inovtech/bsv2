@extends('layouts.client')
@section('content')
<header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
    <div class="header-wrapper sticky-area border-bottom">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="{{url('/')}}">
                        <img src="{{asset('images/Logos.png')}}" alt="TheDir">
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true" data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'><i class="far fa-search"></i></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="{{url('/')}}">
                        <img src="{{asset('images/Logos.png')}}" alt="TheDir">
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>

<div id="wrapper-content" class="wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
        @include('Client.Layouts.sidebar')

        <div class="page-container">
                    <div class="container-fluid">
                        <div class="page-content-wrapper d-flex flex-column">
                            <h1 class="font-size-h4 mb-4 font-weight-bold">Mes Boutiques</h1>
                            <div class="page-content">
                                <div class="tabs">
                                    <ul class="nav nav-pills tab-style-01 font-size-lg" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all"
                                                role="tab" aria-controls="all" aria-selected="true">Toutes mes Boutiques (15)
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="active-tab" data-toggle="tab" href="#active"
                                                role="tab" aria-controls="active" aria-selected="false">Boutiques Active
                                                (12) </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pending-tab" data-toggle="tab" href="#pending"
                                                role="tab" aria-controls="pending" aria-selected="false">Boutiques En attente
                                                Boutiques
                                                (1) </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                                        <div class="store-listing-style-04">
                                            @foreach ($boutiques as $boutique)
                                                <div class="store-listing-item">
                                                    <div class="d-flex align-items-center flex-wrap flex-lg-nowrap border-bottom py-4 py-lg-0">
                                                        <div class="store media align-items-stretch py-">
                                                            <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}" target="_blank" class="store-image">
                                                                @if ($boutique->photo_boutique == null)
                                                                    <img src="{{asset('images/bs.jpeg')}}" alt="Boutique-Senegal">
                                                                @else
                                                                    <img src="{{asset($boutique->photo_boutique)}}" alt="{{$boutique->nom_boutique}}">
                                                                @endif
                                                            </a>
                                                            <div class="media-body px-0 pt-4 pt-md-0">
                                                                <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}" target="_blank"
                                                                    class="font-size-lg font-weight-semibold text-dark d-inline-block mb-2 lh-1"><span
                                                                        class="letter-spacing-25">{{$boutique->nom_boutique}}</span>
                                                                </a>
                                                                <ul
                                                                    class="list-inline store-meta mb-3 font-size-sm d-flex align-items-center flex-wrap">
                                                                    <li class="list-inline-item">
                                                                        <span class="badge badge-success d-inline-block mr-1">5.0</span>
                                                                        <span class="number">4 Produits</span>
                                                                    </li>
                                                                    <li class="list-inline-item separate"></li>
                                                                    <li class="list-inline-item">
                                                                        <span class="mr-1">Pack</span>
                                                                        <span class="text-danger font-weight-semibold">{{$boutique->nom_pack_abonnement}}</span>
                                                                    </li>
                                                                    <li class="list-inline-item separate"></li>
                                                                    <li class="list-inline-item"><a href="#"
                                                                            class="link-hover-secondary-primary">
                                                                            <svg class="icon icon-envelope">
                                                                                <use xlink:href="#icon-envelope"></use>
                                                                            </svg>
                                                                            <span>{{$boutique->email_boutique}}</span>
                                                                        </a></li>
                                                                    <li class="list-inline-item separate"></li>
                                                                    <li class="list-inline-item"><a href="#"
                                                                            class="link-hover-secondary-primary">
                                                                            <svg class="icon icon-cog">
                                                                                <use xlink:href="#icon-cog"></use>
                                                                            </svg>
                                                                            <span>{{$boutique->telephone_boutique}}</span>
                                                                        </a></li>
                                                                </ul>
                                                                <div class="border-top pt-2 d-flex">
                                                                    <span class="d-inline-block mr-1"><i
                                                                            class="fal fa-map-marker-alt">
                                                                        </i>
                                                                    </span>
                                                                    <a href="#"
                                                                        class="text-secondary text-decoration-none address">
                                                                        {{$boutique->adresse_boutique}}
                                                                    </a>
                                                                    <div class="ml-0 ml-sm-auto">
                                                                        <span class="label">Status:</span>
                                                                        <span class="status active">Active</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="action ml-0 ml-lg-auto mt-3 mt-lg-0 align-items-center flex-wrap flex-sm-nowrap w-100 w-lg-auto">
                                                            <a href="{{ route('client.edit-shop', [$boutique->id]) }}" class="btn btn-info btn-icon mb-2 mb-sm-0 font-size-md">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            {{-- <a href="#"
                                                                class="btn btn-primary btn-icon-left mb-2 mb-sm-0 px-5 font-size-md">
                                                                <i class="fal fa-trash-alt"></i>
                                                                Delete
                                                            </a> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <ul class="pagination pagination-style-02">
                                            {{$boutiques->links()}}
                                        </ul>
                                    </div>
                                    <div class="tab-pane fade" id="active" role="tabpanel" aria-labelledby="active-tab">
                                        <div class="store-listing-style-04">
                                           @foreach ($boutiques as $boutique)
                                                <div class="store-listing-item">
                                                    @if ($boutique->is_active_boutique == 1)
                                                        <div class="d-flex align-items-center flex-wrap flex-lg-nowrap border-bottom py-4 py-lg-0">
                                                            <div class="store media align-items-stretch py-">
                                                                <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}" target="_blank" class="store-image">
                                                                    @if ($boutique->photo_boutique == null)
                                                                        <img src="{{asset('images/bs.jpeg')}}" alt="Boutique-Senegal">
                                                                    @else
                                                                        <img src="{{asset($boutique->photo_boutique)}}" alt="{{$boutique->nom_boutique}}">
                                                                    @endif
                                                                </a>
                                                                <div class="media-body px-0 pt-4 pt-md-0">
                                                                    <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}" target="_blank"
                                                                        class="font-size-lg font-weight-semibold text-dark d-inline-block mb-2 lh-1"><span
                                                                            class="letter-spacing-25">{{$boutique->nom_boutique}}</span>
                                                                    </a>
                                                                    <ul
                                                                        class="list-inline store-meta mb-3 font-size-sm d-flex align-items-center flex-wrap">
                                                                        <li class="list-inline-item"><span
                                                                                class="badge badge-success d-inline-block mr-1">5.0</span><span
                                                                                class="number">4 Poroduits</span>
                                                                        </li>
                                                                        <li class="list-inline-item separate"></li>
                                                                        <li class="list-inline-item"><span
                                                                                class="mr-1">Pack</span><span
                                                                                class="text-danger font-weight-semibold">{{$boutique->pack_boutique_id}}</span>
                                                                        </li>
                                                                        <li class="list-inline-item separate"></li>
                                                                        <li class="list-inline-item"><a href="#"
                                                                                class="link-hover-secondary-primary">
                                                                                <svg class="icon icon-envelope">
                                                                                    <use xlink:href="#icon-cog"></use>
                                                                                </svg>
                                                                                <span>{{$boutique->email_boutique}}</span>
                                                                            </a></li>
                                                                        <li class="list-inline-item separate"></li>
                                                                        <li class="list-inline-item"><a href="#"
                                                                                class="link-hover-secondary-primary">
                                                                                <svg class="icon icon-cog">
                                                                                    <use xlink:href="#icon-cog"></use>
                                                                                </svg>
                                                                                <span>{{$boutique->telephone_boutique}}</span>
                                                                            </a></li>
                                                                    </ul>
                                                                    <div class="border-top pt-2 d-flex">
                                                                        <span class="d-inline-block mr-1"><i
                                                                                class="fal fa-map-marker-alt">
                                                                            </i>
                                                                        </span>
                                                                        <a href="#"
                                                                            class="text-secondary text-decoration-none address">
                                                                            {{$boutique->adresse_boutique}}
                                                                        </a>
                                                                        <div class="ml-0 ml-sm-auto">
                                                                            <span class="label">Status:</span>
                                                                            <span class="status active">Active</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="action ml-0 ml-lg-auto mt-3 mt-lg-0 align-items-center flex-wrap flex-sm-nowrap w-100 w-lg-auto">
                                                                <a href="{{ route('client.edit-shop', [$boutique->id]) }}" class="btn btn-info btn-icon mb-2 mb-sm-0 font-size-md">
                                                                    <i class="fa fa-edit"></i>
                                                                </a>
                                                                {{-- <a href="#"
                                                                    class="btn btn-primary btn-icon-left mb-2 mb-sm-0 px-5 font-size-md">
                                                                    <i class="fal fa-trash-alt"></i>
                                                                    Delete
                                                                </a> --}}
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pending" role="tabpanel"
                                        aria-labelledby="pending-tab">
                                        <div class="store-listing-style-04">
                                            @foreach ($boutiques as $boutique)
                                                <div class="store-listing-item">
                                                    @if ($boutique->is_active_boutique == 0)
                                                        <div class="d-flex align-items-center flex-wrap flex-lg-nowrap border-bottom py-4 py-lg-0">

                                                            <div class="store media align-items-stretch py-4">
                                                                    <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}" target="_blank" class="store-image">
                                                                            @if ($boutique->photo_boutique == null)
                                                                                <img src="{{asset('images/bs.jpeg')}}" alt="Boutique-Senegal">
                                                                            @else
                                                                                <img src="{{asset($boutique->photo_boutique)}}" alt="{{$boutique->nom_boutique}}">
                                                                            @endif
                                                                    </a>
                                                                    <div class="media-body px-0 pt-4 pt-md-0">
                                                                        <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}" target="_blank"
                                                                            class="font-size-lg font-weight-semibold text-dark d-inline-block mb-2 lh-1"><span
                                                                                class="letter-spacing-25">
                                                                                @if ($boutique->is_active_boutique == 0)
                                                                                    {{$boutique->nom_boutique}}
                                                                                @endif
                                                                            </span>
                                                                        </a>
                                                                        <ul
                                                                            class="list-inline store-meta mb-3 font-size-sm d-flex align-items-center flex-wrap">
                                                                                <li class="list-inline-item"><span
                                                                                        class="badge badge-success d-inline-block mr-1">5.0</span><span
                                                                                        class="number">4 Poroduits</span>
                                                                                </li>
                                                                            <li class="list-inline-item separate"></li>
                                                                            <li class="list-inline-item"><span
                                                                                    class="mr-1">Pack</span><span
                                                                                    class="text-danger font-weight-semibold">{{$boutique->pack_boutique_id}}</span>
                                                                            </li>
                                                                            <li class="list-inline-item separate"></li>
                                                                            <li class="list-inline-item"><a href="#"
                                                                                    class="link-hover-secondary-primary">
                                                                                    <svg class="icon icon-envelope">
                                                                                        <use xlink:href="#icon-cog"></use>
                                                                                    </svg>
                                                                                    <span>
                                                                                        {{$boutique->email_boutique}}
                                                                                    </span>
                                                                                </a></li>
                                                                            <li class="list-inline-item separate"></li>
                                                                            <li class="list-inline-item"><a href="#"
                                                                                    class="link-hover-secondary-primary">
                                                                                    <svg class="icon icon-cog">
                                                                                        <use xlink:href="#icon-cog"></use>
                                                                                    </svg>
                                                                                    <span>
                                                                                        {{$boutique->telephone_boutique}}
                                                                                    </span>
                                                                                </a></li>
                                                                        </ul>
                                                                        <div class="border-top pt-2 d-flex">
                                                                            <span class="d-inline-block mr-1"><i
                                                                                    class="fal fa-map-marker-alt">
                                                                                </i>
                                                                            </span>
                                                                            <a href="#"
                                                                                class="text-secondary text-decoration-none address">
                                                                                    {{$boutique->adresse_boutique}}
                                                                            </a>
                                                                                <div class="ml-0 ml-sm-auto">
                                                                                    <span class="label">Status:</span>
                                                                                    <span class="status active">desactive</span>
                                                                                </div>
                                                                        </div>
                                                                    </div>

                                                            </div>
                                                            <div
                                                                class="action ml-0 ml-lg-auto mt-3 mt-lg-0 align-items-center flex-wrap flex-sm-nowrap w-100 w-lg-auto">
                                                                <a href="{{ route('client.edit-shop', [$boutique->id]) }}" class="btn btn-info btn-icon mb-2 mb-sm-0 font-size-md">
                                                                    <i class="fa fa-edit"></i>
                                                                </a>
                                                                {{-- <a href="#"
                                                                    class="btn btn-primary btn-icon-left mb-2 mb-sm-0 px-5 font-size-md">
                                                                    <i class="fal fa-trash-alt"></i>
                                                                    Delete
                                                                </a> --}}
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-5">
                            <a href="/" class="link-hover-dark-primary font-weight-semibold">Boutique Senegal</a>
                                Tout droit réservé. Développé par
                            <a href="https://inovtechsenegal.com" class="link-hover-dark-primary font-weight-semibold">Inovtech</a>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
</div>

@foreach($boutiques as $boutique)
<!-- Modal Update Statut -->
<div class="modal fade" id="boutiqueStatut{{$boutique->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h5 class="text-uppercase" id="updateModalLongTitle">
                    @if($boutique->is_active_boutique == 0)
                        Activation boutique
                    @elseif($boutique->is_active_boutique == 1)
                        Désactivation boutique
                    @endif
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('boutique.statut') }}" method="post">
                @csrf
                @method('GET')
                <div class="modal-body" >
                    <input type="hidden" name="id" value="{{$boutique->id}}">
                    <input type="hidden" name="is_active_boutique" value="{{$boutique->is_active_boutique}}">
                    <p class="text-center" id="updateModalLongBody" style="font-size:16px">
                        @if($boutique->is_active_boutique == 0)
                            Etes-vous sûr de vouloir activer cette boutique ?
                        @elseif($boutique->is_active_boutique == 1)
                            Etes-vous sûr de vouloir désactiver cette boutique ?
                        @endif
                    </p>
                </div>
                <div class="modal-footer">
                    @if($boutique->is_active_boutique == 0)
                        <button type="submit" class="btn btn-success mr-1">Activer</button>
                    @elseif($boutique->is_active_boutique == 1)
                        <button type="submit" class="btn btn-danger mr-1">Désactiver</button>
                    @endif
                    <button type="close" class="btn btn-secondary" data-dismiss="modal" >Annuler</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endforeach

<!-- Modal -->
    <div class="modal fade" id="modalDeletePack" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header bg-light">
                    <h5 class="text-uppercase" id="exampleModalLongTitle"><i class="mdi mdi-account-circle mr-1"></i>
                            Suppression Categorie Blog
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="deleteForm" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                <div class="modal-body">
                        <p class="text-center" style="font-size:16px">Etes-vous sûr de vouloir supprimer cette Boutique ?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger mr-1" data-dismiss="modal" onclick="formSubmit()">Supprimer</button>
                    <button type="close" class="btn btn-success" data-dismiss="modal" >Annuler</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @section('script')
    <script type=text/javascript>
        function deleteData(id)
        {
            var id = id;
            var url = '{{ route("categorie-blog.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#deleteForm").attr('action', url);
        }

        function formSubmit()
        {
            $("#deleteForm").submit();
        }
    </script>
@endsection

@endsection
