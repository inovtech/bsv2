<div class="sidebar">
    <div class="container-fluid">
        <div class="user-profile media align-items-center mb-6">
            <div class="image mr-3">
                @if (Auth::user()->profile_photo_path == null)
                    <a href="{{url('/profil')}}">
                        <img src="{{asset('images/bs.jpeg')}}" alt="Boutique-Senegal" class="rounded-circle">
                    </a>
                @else
                    <a href="{{url('/profil')}}">
                        <img src="{{asset(Auth::user()->profile_photo_path)}}" alt="{{Auth::user()->nom_user}}" class="rounded-circle">
                    </a>
                @endif
            </div>
                <div class="media-body lh-14">
                <span class="text-dark d-block font-size-md">Salam,</span>
                <span class="mb-0 h5">{{ Auth::user()->prenom_user }} {{ Auth::user()->nom_user }}!</span>
            </div>
        </div>
        <ul class="list-group list-group-flush list-group-borderless">
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="{{route("client.create-shop")}}" class="d-flex align-items-center btn btn-primary font-size-md">
                    <span class="d-inline-block mr-3"><i class="fal fa-plus"></i></span>
                    <span>Ajouter une boutique</span>
                </a>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="{{url("/dashboard")}}" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                    <span class="d-inline-block mr-3"><i class="fal fa-tachometer"></i></span>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="#listing" class="d-flex align-items-center link-hover-dark-primary font-size-md" data-toggle="collapse" aria-expanded="false">
                    <span class="d-inline-block mr-3"><i class="fal fa-store"></i></span>
                    <span>Boutique</span>
                    <span class=" ml-auto"><i class="fal fa-chevron-down"></i></span>
                </a>
                <ul class="submenu collapse list-group list-group-flush list-group-borderless pt-2 mb-0 sidebar-menu" id="listing">
                    <li class="list-group-item p-0 mb-2 lh-15">
                        <a href="{{url("/boutiques")}}" class="link-hover-dark-primary font-size-md">Liste</a>
                    </li>
                    <li class="list-group-item p-0 mb-2 lh-15">
                        <a href="{{url("/boutiques/create")}}" class="link-hover-dark-primary font-size-md">Nouvelle</a>
                    </li>
                </ul>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="{{url("/produit")}}" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                    <span class="d-inline-block mr-3"><i class="fal fa-shopping-bag"></i></span>
                    <span>Produit</span>
                </a>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="{{url('/profil')}}" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                    <span class="d-inline-block mr-3"><svg class="icon icon-user"><use xlink:href="#icon-user"></use></svg></span>
                    <span>Mon Profile</span>
                </a>
            </li>
            <li class="list-group-item p-0 mb-2 lh-15">
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" class="d-flex align-items-center link-hover-dark-primary font-size-md">
                    <span class="d-inline-block mr-3"><svg class="icon icon-exit"><use xlink:href="#icon-exit"></use></svg></span>
                    <span>Logout</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</div>
